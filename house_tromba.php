<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Tromba</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/tromba/image1.jpg',
          'images/tromba/image2.jpg',
          'images/tromba/image3.jpg',
          'images/tromba/image4.jpg',
          'images/tromba/image5.jpg',
          'images/tromba/image6.jpg',
          'images/tromba/image7.jpg',
          'images/tromba/image8.jpg',
          'images/tromba/image9.jpg',
          'images/tromba/image10.jpg',
        ]);
      });
      imgDir = "tromba";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <?php
            include("sidebarHousing.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php'>Accomodations</a> » Tromba
          </div>
          <h1>Tromba</h1>
        <div class='imagePreview'>
          <img id='newImage' src='images/tromba/image1.jpg' class='previewLoader'/>
          <img id='oldImage' src='images/tromba/image1.jpg'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/tromba/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/tromba/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/tromba/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/tromba/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/tromba/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/tromba/image6.png' onclick='changePreview(6)' /></li>
                <li><img src='images/tromba/image7.png' onclick='changePreview(7)' /></li>
                <li><img src='images/tromba/image8.png' onclick='changePreview(8)' /></li>
                <li><img src='images/tromba/image9.png' onclick='changePreview(9)' /></li>
                <li><img src='images/tromba/image10.png' onclick='changePreview(10)' /></li>
                <li><img src='images/tromba/image11.png' onclick='changePreview(11)' /></li>
                <li><img src='images/tromba/image12.png' onclick='changePreview(12)' /></li>
                <li><img src='images/tromba/image13.png' onclick='changePreview(13)' /></li>
              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>Unit Details:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Type of accomodations: </td>
                <td>2-rooms apartment, 35m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of beds :</td>
                <td>4 (2+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bedrooms :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bathrooms :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Floor / terrace :</td>
                <td>1<sup>st</sup> floor, shared terrace (100 m²) on ground floor with table, chairs and deckchair.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Location :</td>
                <td>Countryside with a shared garden of 5000 m² in a property of 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composed of :</td>
                <td>Kitchen, 1 double bedroom (king size bed with MEMOREX foam), 1 bathroom with shower</td>
              </tr>
              <tr>
                <td class='tdHeader'>Kitchen corner equipped with :</td>
                <td>Dishwasher, refrigerator, freezer, 4 kitchen stoves (gas) and a toaster.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Apartment equipped with :</td>
                <td>Air conditioning, Heating, LCD digital TV, and hairdryer.</td>
              </tr>
              <tr>
                <td class='tdHeader'>View :</td>
                <td>Countryside</td>
              </tr>
              <tr>
                <td class='tdHeader'>Access of disabled / bathroom for disabled :</td>
                <td>No</td>
              </tr>
            </table>

			<h2>Tromba Plus - This room can be combined with <a href="house_camera1.php">Blue Room</a>:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Type of accomodations :</td>
                <td>3-rooms apartment, 58m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of beds :</td>
                <td>6 (4+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bedrooms :</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bathrooms :</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>Floor / terrace :</td>
                <td>1<sup>st</sup> floor, shared terrace (100 m²) on ground floor with table, chairs and deckchair.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Location :</td>
                <td>Countryside with a shared garden of 5000 m² in a property of 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composed of :</td>
                <td>Kitchen, 2 double bedrooms with king size bed with MEMOREX foam, 2 bathroom with shower.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Kitchen corner equipped with :</td>
                <td>Dishwasher, refrigerator, freezer, 4 kitchen stoves (gas) and a toaster.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Apartment equipped with :</td>
                <td>Air conditioning, Heating, LCD digital TV, and hairdryer.</td>
              </tr>
              <tr>
                <td class='tdHeader'>View :</td>
                <td>Countryside</td>
              </tr>
              <tr>
                <td class='tdHeader'>Access of disabled / bathroom for disabled :</td>
                <td>No</td>
              </tr>
            </table>
            <p>*Please note that the apartment 14 is composed of Apartment Tromba and Blue Room they are both on the first floor one beside the other but they have a separate entrance and there is no connection door.</p>


            <h2>Swimming Pool:</h2>
            <img src='images/pool1.jpg'/>
            <table>
              <tr>
                <td class='tdHeader'>Dimensions :</td>
                <td>14m x 7m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Depth :</td>
                <td>Minimum 1m; maximum 1.40m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Opening period :</td>
                <td>From March 31 till October 13</td>
              </tr>
              <tr>
                <td class='tdHeader'>Opening hours :</td>
                <td>From 8.30h until 18.30h</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pool cleaning :</td>
                <td>Daily</td>
              </tr>
              <tr>
                <td class='tdHeader'>Whirlpool :</td>
                <td>Shared, dimensions 2,20m x 2,20m, deep 0,50 m</td>
              </tr>
            </table>

            <h2>Services:</h2>
            A laundry room with washing machine, dryer and iron, free wi-fi, parking place, a children’s playing area (in construction, ready for the season 2012), 2 shared barbecues and an outside shower in the swimming pool.
            <br/><br/>
            <a href='reservation.php?unit=Tromba'><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
