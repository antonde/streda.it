<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Accomodations</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-16px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>Our Apartments</h1>
            <p>The place was created around the philosophy that states: "if arts and culture are the essence of the human being, wine is the essence and glorification of the ground".</p>
			<p>Our locale is perfect for couples, families with children and small groups seeking a cultural and relaxing break.</p>
			<p>There are 4 rooms and 13 one-to-two bedroom apartments, each equipped with a kitchenette and located within 2 rural farmhouses. Recently renovated with an eye to ancient Tuscan traditions.</p> 
            <p>All accommodation enjoy the comforts of modern technology including individual air conditioning & heating, widescreen digital TV, free Wi-Fi, dishwasher, and fridge and freezer.  Fresh cotton linens are provided and mattress are cover in MEMOREX foam to give you a comfortable night's sleep.  You also have your choice of foam or down pillows.</p> 
                        
			<p>Outside, there is a beautiful terrace and pool.</p>
            <div style='margin:auto auto;'>
              <h2>Casa Belvedere</h2>
				<p>This building consists of 5 spacious apartments furnished in the Classic Tuscan style; with marble sinks, marble-topped tables and spring colored furniture for a more enjoyable stay. Most of the apartments enjoy magnificent views over the terrace, swimming pool and landscape.</p>
             
             <div style="width:50%; float: left;">
             
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_merlot.php'><img class="apartment" src='images/merlot/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_merlot.php'>Apartment 1: <span>Merlot</span></a></h3>
                    <a href='house_merlot.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_cabernet.php'><img class="apartment" src='images/cabernet/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_cabernet.php'>Apartment 2: <span>Cabernet</span></a></h3>
                    <a href='house_cabernet.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_syrah.php'><img class="apartment" src='images/syrah/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_syrah.php'>Apartment 3: <span>Syrah</span></a></h3>
                    <a href='house_syrah.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>              

			</div>
            <div style="width:50%; float: left;">
            


              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_sangiovese.php'><img class="apartment" src='images/sangiovese/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_sangiovese.php'>Apartment 4: <span>Sangiovese</span></a></h3>
                    <a href='house_sangiovese.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_chardonnay.php'><img class="apartment" src='images/chardonnay/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_chardonnay.php'>Apartment 5: <span>Chardonnay</span></a></h3>
                    <a href='house_chardonnay.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>                
            
            
            </div>
            <div class="clear"></div>
            
            
            
            
            </div>
            
            
            
            
            
            
            
            <div style='margin:auto auto;'>
              <h2>Casa Clarinetto</h2>
				<p>This building includes 4 apartments and 4 rooms, all furnished in the Miami Swing collection which was inspired by the well known artist and singer Renzo Arbore and designed by award winning designers Licheri and Cappellini who are behind reputable theatre and television productions including those for RAI, the state-run Italian tv-channel. With its unusual interiors, Casa Clarinetto's jazz and pop art inspired decor is fresh and vibrant.</p>
              
              
              <div style="width:50%; float: left;">
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_clarinetto.php'><img class="apartment" src='images/clarinetto/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_clarinetto.php'>Apartment 10: <span>Clarinetto</span></a></h3>
                    <a href='house_clarinetto.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_sassophone.php'><img class="apartment" src='images/sassophone/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_sassophone.php'>Apartment 11: <span>Sassofono</span></a></h3>
                    <a href='house_sassophone.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_orchestra.php'><img class="apartment" src='images/orchestra/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_orchestra.php'>Apartment 12: <span>Orchestra</span></a></h3>
                    <a href='house_orchestra.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>              

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_tromba.php'><img class="apartment" src='images/tromba/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_tromba.php'>Apartment 13: <span>Tromba</span></a></h3>
                    <a href='house_tromba.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>   

				</div>
                <div style="width:50%; float: left;">


              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera1.php'><img class="apartment" src='images/camera1/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera1.php'>Swing 1:<br><span>Blue Room</span></a></h3>
                    <a href='house_camera1.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera2.php'><img class="apartment" src='images/camera2/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera2.php'>Swing 2:<br><span>Orange Room</span></a></h3>
                    <a href='house_camera2.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera3.php'><img class="apartment" src='images/camera3/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera3.php'>Swing 3:<br><span>Violet Room</span></a></h3>
                    <a href='house_camera3.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera4.php'><img class="apartment" src='images/camera4/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera4.php'>Swing 4:<br><span>Green Room</span></a></h3>
                    <a href='house_camera4.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              </div>
              <div class="clear"></div>
              
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
