<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Olive Oil</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebarFarm.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
            <h1>托斯卡纳IGP-超级精选橄榄油</h1>
            <img src='images/oil1.png'/>
            <p>
              产自STREDA BELVEDERE的超级精选橄榄油IGP采用当地正宗的传统技术制造。这种橄榄油适合喜欢托斯卡纳经典香气的顾客，橄榄油产量有限，是维赛兹奥山谷地区的知名品种，历史悠久，比如moraiole, leccine和frantoiane橄榄油。由于富含多酚和其他特殊营养物质，这种橄榄油一直适用于儿童食用。<br><br>可与沙拉和生菜搅拌食用，也是制作托斯卡纳地区最有名的“意式特色面包”的最佳搭配材料，还可用于护肤和其他护理作用。<br><br>产自托斯卡纳Vinci的Streda belvedere IGP橄榄油非常特别，主要受益于当地特色的土地条件和地理构成，当然气候条件和当地数个世纪以来种植和栽培橄榄树的传统有关。这些有利因素使得橄榄油具有无与伦比的感官特性、颜色丰富多彩，随时间变化呈绿色至金黄色不等，还带有成熟杏仁的香味。<br><br> IGP认证证明每一道生产工序，从种植、收获橄榄、压榨和包装都是在托斯卡纳地区进行操作，并且产品的所有理化特征和感官特性都与托斯卡纳地区橄榄油IGP协会的陈述一致。
              <br/></br>
              <img src='images/oil2.png' style='float:right;'/>
              <b>Bottles</b><br/>
              Da 0,750 LT<br/>
              Da 0,500 LT<br/>
              Da 0,250 LT<br/>
              <br/>
              <b>Cans</b><br/>
              Da 5 LT
            <p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
