<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - 农场</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-16px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>主要公寓设施</h1>
            <p>公寓配有空调、可独立控制温度控制、双人床、奢华寝具、包括舒适崭新的棉麻织物、枕垫、羽绒枕、彩电、卫星电视、冰箱和洗碗机。</p>
			<p>可接收无线通信信号。</p> 
                        
			<p>外面有一个美丽的露台和游泳池</p>
            <div style='margin:auto auto;'>
              <h2>Casa Belvedere 观景楼之家</h2>
				<p>屋子内有五个宽敞的公寓，典型的托斯卡纳装修风格，内有大理石洗脸盆、大理石台面的餐桌和色彩明快的家具，适宜居住。从公寓内的阳台向外眺望，可以看到美丽的风景，还有游泳池和景观设施。</p>
             
             <div style="width:50%; float: left;">
             
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_merlot.php'><img class="apartment" src='images/merlot/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_merlot.php'>公寓1：<span>梅乐汁</span></a></h3>
                    <a href='house_merlot.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_cabernet.php'><img class="apartment" src='images/cabernet/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_cabernet.php'>公寓2：<span>赤霞珠</span></a></h3>
                    <a href='house_cabernet.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_syrah.php'><img class="apartment" src='images/syrah/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_syrah.php'>公寓3：<span>席拉</span></a></h3>
                    <a href='house_syrah.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>              

			</div>
            <div style="width:50%; float: left;">
            


              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_sangiovese.php'><img class="apartment" src='images/sangiovese/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_sangiovese.php'>公寓4：<span>桑娇维赛</span></a></h3>
                    <a href='house_sangiovese.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_chardonnay.php'><img class="apartment" src='images/chardonnay/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_chardonnay.php'>公寓5：<span>夏敦埃酒</span></a></h3>
                    <a href='house_chardonnay.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>                
            
            
            </div>
            <div class="clear"></div>
            
            
            
            
            </div>
            
            
            
            
            
            
            
            <div style='margin:auto auto;'>
              <h2>单簧管之家 Casa Clarinetto</h2>
				<p>屋子内有四个公寓和四间房间，装修和家具风格为经典的迈阿密风情，受著名的艺术家和歌唱家Renzo Arbore启发，由获奖设计师Licheri和Cappellini设计，这两位设计师为许多负有盛名的剧院和意大利州立电视频道RAI设计了不少影视作品。在独树一帜的装饰物的点缀下，单簧管之家的装饰风格结合了爵士和流行乐，清新明快</p>
              
              
              <div style="width:50%; float: left;">
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_clarinetto.php'><img class="apartment" src='images/clarinetto/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_clarinetto.php'>公寓10：<span>单簧管</span></a></h3>
                    <a href='house_clarinetto.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_sassophone.php'><img class="apartment" src='images/sassophone/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_sassophone.php'>公寓11：<span>萨克斯</span></a></h3>
                    <a href='house_sassophone.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_orchestra.php'><img class="apartment" src='images/orchestra/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_orchestra.php'>公寓12：<span>管弦乐队</span></a></h3>
                    <a href='house_orchestra.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>              

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_tromba.php'><img class="apartment" src='images/tromba/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_tromba.php'>公寓13：<span>小号</span></a></h3>
                    <a href='house_tromba.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>   

				</div>
                <div style="width:50%; float: left;">


              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera1.php'><img class="apartment" src='images/camera1/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera1.php'>1 号旋律：<br><span>蓝色房间</span></a></h3>
                    <a href='house_camera1.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera2.php'><img class="apartment" src='images/camera2/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera2.php'>2 号旋律：<br><span>橙色房间</span></a></h3>
                    <a href='house_camera2.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera3.php'><img class="apartment" src='images/camera3/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera3.php'>3 号旋律：<br><span>紫色房间</span></a></h3>
                    <a href='house_camera3.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera4.php'><img class="apartment" src='images/camera4/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera4.php'>4 号旋律：<br><span>绿色房间</span></a></h3>
                    <a href='house_camera4.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              </div>
              <div class="clear"></div>
              
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
