<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Orchestra</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/orchestra/image1.jpg',
          'images/orchestra/image2.jpg',
          'images/orchestra/image3.jpg',
          'images/orchestra/image4.jpg',
          'images/orchestra/image5.jpg',
          'images/orchestra/image6.jpg',
          'images/orchestra/image7.jpg',
          'images/orchestra/image8.jpg',
          'images/orchestra/image9.jpg',
          'images/orchestra/image10.jpg'
        ]);
      });
      imgDir = "orchestra";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <?php
            include("sidebarHousing.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php'>住宿条件</a> » 管弦乐队
          </div>
          <h1>管弦乐队</h1>
        <div class='imagePreview'>
          <img id='newImage' src='images/orchestra/image1.jpg' class='previewLoader'/>
          <img id='oldImage' src='images/orchestra/image1.jpg'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/orchestra/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/orchestra/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/orchestra/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/orchestra/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/orchestra/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/orchestra/image6.png' onclick='changePreview(6)' /></li>
                <li><img src='images/orchestra/image7.png' onclick='changePreview(7)' /></li>
                <li><img src='images/orchestra/image8.png' onclick='changePreview(8)' /></li>
                <li><img src='images/orchestra/image9.png' onclick='changePreview(9)' /></li>
                <li><img src='images/orchestra/image10.png' onclick='changePreview(10)' /></li>
              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>房间详情：</h2>
            <table>
              <tr>
                <td class='tdHeader'>房间类型：</td>
                <td>2-rooms apartment, 56m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>床：</td>
                <td>4 (2+2)张</td>
              </tr>
              <tr>
                <td class='tdHeader'>卧室：</td>
                <td>1 间</td>
              </tr>
              <tr>
                <td class='tdHeader'>浴室：</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>楼层/平台：</td>
                <td>一层；私人露台（40 mq），设有餐桌、椅子和躺椅。</td>
              </tr>
              <tr>
                <td class='tdHeader'>位置：</td>
                <td>5000 m²的乡间花园，整个占地面积达80公顷。</td>
              </tr>
              <tr>
                <td class='tdHeader'>内部设施：</td>
                <td>客厅 – 餐厅，一张双人沙发床，烹调区，一间双人房（大号床，MEMOREX床垫），两间浴室，内有淋浴设施。</td>
              </tr>
              <tr>
                <td class='tdHeader'>开放式厨房：</td>
                <td>洗碗机、冰箱、冰柜、4个厨灶（煤气灶）和烤箱。</td>
              </tr>
              <tr>
                <td class='tdHeader'>公寓设施：</td>
                <td>空调、暖气、LCD数码电视和吹风机。</td>
              </tr>
              <tr>
                <td class='tdHeader'>风景：</td>
                <td>乡村美景</td>
              </tr>
              <tr>
                <td class='tdHeader'>残疾人专用浴室：</td>
                <td>Yes</td>
              </tr>
            </table>
			<h2>管弦乐队之间特色 – 这间房间可以与紫色房间相结合： <a href="house_camera3.php" target="_blank">Violet Room</a>:</h2>
            <table>
              <tr>
                <td class='tdHeader'>房间类型：</td>
                <td>3-rooms apartment, 75m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>床：</td>
                <td>6 (4+2)张</td>
              </tr>
              <tr>
                <td class='tdHeader'>卧室：</td>
                <td>2 间（一间房间位于二楼，独立入口，无相连的门）。</td>
              </tr>
              <tr>
                <td class='tdHeader'>浴室：</td>
                <td>3</td>
              </tr>
              <tr>
                <td class='tdHeader'>楼层/平台：</td>
                <td>一层和二层；私人露台（40 mq），设有餐桌、椅子和躺椅。</td>
              </tr>
              <tr>
                <td class='tdHeader'>位置：</td>
                <td>5000 m²的乡间花园，整个占地面积达80公顷。</td>
              </tr>
              <tr>
                <td class='tdHeader'>内部设施：</td>
                <td>一层：客厅/餐厅，一张双人沙发床，一间小厨房，一张双人床（特大号床，MEMOREX床垫），两间浴室，内有淋浴设施；二层（独立入口，无相连的门）：一张双人床（特大号，MEMOREX床垫），一间浴室，内有淋浴设施。</td>
              </tr>
              <tr>
                <td class='tdHeader'>开放式厨房：</td>
                <td>洗碗机、冰箱、冰柜、4个厨灶（煤气灶）和烤箱。</td>
              </tr>
              <tr>
                <td class='tdHeader'>公寓设施：</td>
                <td>空调、暖气、LCD数码电视和吹风机。</td>
              </tr>
              <tr>
                <td class='tdHeader'>风景：</td>
                <td>乡村美景</td>
              </tr>
              <tr>
                <td class='tdHeader'>残疾人专用浴室：</td>
                <td>有（仅在公寓内可以享有，2层的房间内无此设施）。</td>
              </tr>
            </table>
            <p>*请注意管弦乐队之家在一层，紫色房间在二层，两者有各自的独立入口，无相连的门。因此，此公寓仅适用于成人，或能够独立居住的儿童和家长居住。</p>            

            <h2>游泳池：</h2>
            <table>
              <tr>
                <td class='tdHeader'>面积：</td>
                <td>14m x 7m</td>
              </tr>
              <tr>
                <td class='tdHeader'>深度：</td>
                <td>最浅处1m，最深处1.40m</td>
              </tr>
              <tr>
                <td class='tdHeader'>开发月份：</td>
                <td>从3月31日至10月31日开放</td>
              </tr>
              <tr>
                <td class='tdHeader'>开发月份：</td>
                <td>从3月31日至10月31日开放</td>
              </tr>
              <tr>
                <td class='tdHeader'>泳池清洁：</td>
                <td>每日</td>
              </tr>
              <tr>
                <td class='tdHeader'>漩涡：</td>
                <td>共享式，面积2,20m x 2,20m，深度0,50 m</td>
              </tr>
            </table>

            <h2>服务：</h2>
            配有洗衣机、烘干机和熨斗的洗衣房，免费wi-fi，停车场，儿童游戏区（正在建设中，2012年可以开放），两个共享烤肉架，游泳池外的淋浴房。
            <br/><br/>
            <a href='reservation.php?unit=Orchestra'><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
