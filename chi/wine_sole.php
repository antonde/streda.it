<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Sole Chianti Superiore DOCG</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Our Wine</a> » Sole Chianti Superiore DOCG
          </div>        
          <div class='post'>
            <h1>Sole di Streda Belvedere</h1>
            <p>
              <h2>Chianti superiore dop(docg)</h2>
            <p>
            <img src='images/wines/wine_sole.png' style='float:left;'/>
            <b>产地特征</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>生产商</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄种类</td>
                <td>90% 桑娇维赛和其他法律授权的葡萄</td>
              </tr>
              <tr>
                <td class='tdHeader'>海拔</td>
                <td>200米</td>
              </tr>
              <tr>
                <td class='tdHeader'>方向和土壤面貌</td>
                <td>自东向西方向、中等土质、带有大量粘土、带有钙化化石成分。</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄园密度</td>
                <td>4500 个葡萄藤/每公顷</td>
              </tr>
              <tr>
                <td class='tdHeader'>培育方法</td>
                <td>带刺的包围隔离</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄藤平均成熟时间</td>
                <td>15 年</td>
              </tr>
            </table>
            <b>发酵和成熟</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>每公顷的产量</td>
                <td>90 公担</td>
              </tr>
              <tr>
                <td class='tdHeader'>收获期</td>
                <td>九月中旬、机器收割</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄加工</td>
                <td>压碎，去梗</td>
              </tr>
              <tr>
                <td class='tdHeader'>发酵温度和时间</td>
                <td>在28摄氏度的不锈钢发酵罐中、存放20天时间。</td>
              </tr>
              <tr>
                <td class='tdHeader'>外皮浸渍作用</td>
                <td>12 天</td>
              </tr>
             <tr>
                <td class='tdHeader'>乳酸发酵</td>
                <td>是</td>
              </tr>
              <tr>
                <td class='tdHeader'>成熟期</td>
                <td>在水泥酒桶内存放8个月时间</td>
              </tr>
              <tr>
                <td class='tdHeader'>酒精度</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>最小装瓶陈化时间</td>
                <td>三个月</td>
              </tr>
            </table>
            <b>品尝体验</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>外观</td>
                <td>红宝石色</td>
              </tr>
              <tr>
                <td class='tdHeader'>嗅觉感受</td>
                <td>红色果香、紫罗兰果香、非常激烈的味道，</td>
              </tr>
              <tr>
                <td class='tdHeader'>味觉感受</td>
                <td>很平衡、 甜丹宁酸、具有较好的酸度、多汁、优雅味道</td>
              </tr>
              <tr>
                <td class='tdHeader'>适宜搭配</td>
                <td>肉类、开胃菜、沙拉和新鲜的奶酪。</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>

