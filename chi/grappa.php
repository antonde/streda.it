<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Grappa</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>基安蒂红葡萄酒 Grappa del Chianti</h1>
            <h1>GOCCE DI LUNA</h1>
            <img src='images/grappa1.png'/>
            <p>
              我们采用产自Streda庄园的芳香四溢的DOGG基安蒂葡萄，使用葡萄的表皮、茎干和种子制造格拉巴酒。通过发酵作用和地下酒窖里的蒸馏作用，充分保留葡萄的原汁原味和浓醇香气。
              <br/><br/>
              <b>感官特性：</b><br/>
              口感丰富，花香与果味完美融合，淡淡芳香。 
              <br/><br/>
              <b>口味：</b><br/>
              清淡柔滑
              <br/><br/>
              <b>酒精度</b><br/>
              40°
              <br/><br/>
              <b>酒瓶含量</b><br/>
              Size/ml 700
            <p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
