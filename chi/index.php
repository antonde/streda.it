<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Home</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/image1.jpg',
          'images/image2.jpg',
          'images/image3.jpg',
          'images/image4.jpg',
          'images/image5.jpg',
          'images/image6.jpg',
        ]);
      });
      imgDir = "";
    </script>
<?php require_once('meta.php'); ?>


 </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='content'>
        <div class='imagePreview'>
          <img id='oldImage' src='images/image1.jpg'/>
          <img id='newImage' src='images/image1.jpg' class='previewLoader'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/image6.png' onclick='changePreview(6)' /></li>
              </ul>
            </div>
          </div>
        </div>
        <a href='accomodations.php'><img src='images/buttonFancy.png'/></a>
        <div class='post'>
          <h1>浪漫的意大利Borgo</h1>
          <p>
            Streda Belvedere位于Vinci小镇，这里是著名大师莱昂纳多•达芬奇的故乡，位于托斯卡纳市中心地带，距离佛罗伦萨、比萨、圣吉米纳诺和锡耶纳不远。在罗马时期，小镇迅速发展，附近的位于Streda的圣巴托洛梅奥教堂最为著名，其历史大约可以追溯至1000年。
          </p>
          <img src='images/article01.png'/>
          <p>
            近年来，人们已经重建了浪漫的Borgo，人们前所未有地能够有机会花费一整个假期，把身心沉浸在Streda Belvedere一代代人们传承培育的历史悠久的葡萄酒和橄榄树之间。莱昂纳多•达芬奇的父亲，Ser Piero在十六世纪时期，已经开始在自家的园地上种植葡萄藤。
          </p>
          <p>Lenzi家族，Roberto一世和他的儿子Claudio继承了这项家族传统，在自家80公顷的园地上广泛种植葡萄藤和橄榄树。</p>
          <p>lago di Streda正好位于这片园地的中心，而大多数葡萄园都朝着lago di Streda，他们充分利用了这一点绝佳的地理环境优势。此外，他们还挖掘了当地小气候的潜力，这可以算是对葡萄栽培的突出贡献了。</p>
          <p>Claudio Lenzi目前仍然与一批专家共同合作，尤其是与酿酒学家Francesco Bartoletti和农学家Stefano Dini共同研究一个结合了传统和现代技术的项目，致力于获得高品质的纯正的葡萄酒。</p>
          <p>为了获得比传统品种质量更佳的葡萄酒，除了Sangiovese的多种克隆品种，他们还种植了许多国际化的品种，比如Syrah, Merlot，Cabernet和Chardonnay。</p>
          <p><strong>饮用之前，我会闻一闻，闻一闻这纯洁的诱人酒香。
你觉可笑？我所闻的是葡萄酒如花的芬芳。</br>
——Amaury de Cazanove</strong></p>

          <a href='history.php'>了解关于Streda Belvedere的更多信息</a>, or <a href='accomodations.php'>或点击此处了解</a>.
        </div>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
