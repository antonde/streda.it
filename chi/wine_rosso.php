<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Duccio Rosso</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>我们的葡萄酒</a> » Duccio Rosso
          </div>        
          <div class='post'>
            <h1>Duccio di Streda Belvedere Rosso</h1>
            <p>
              <h2>Toscana Rosso igp(igt)</h2>
            <p>
            <img src='images/wines/rosso.png' style='float:left;'/>
            <b>产地特征</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>生产商</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄种类：</td>
                <td>主要桑娇维赛</td>
              </tr>
              <tr>
                <td class='tdHeader'>海拔</td>
                <td>200 米</td>
              </tr>
              <tr>
                <td class='tdHeader'>方向和土壤面貌</td>
                <td>自东向西方向。中等土质。</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄园密度</td>
                <td>4000 个葡萄藤/每公顷</td>
              </tr>
              <tr>
                <td class='tdHeader'>培育方法</td>
                <td>带刺的包围隔离</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄藤平均成熟时间</td>
                <td>15 年</td>
              </tr>
            </table>
            <b>发酵和成熟</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>每公顷的产量</td>
                <td>110 公担</td>
              </tr>
              <tr>
                <td class='tdHeader'>收获期</td>
                <td>九月、机器收割</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄加工</td>
                <td>压碎、去梗。</td>
              </tr>
              <tr>
                <td class='tdHeader'>发酵温度和时间</td>
                <td>在32摄氏度的不锈钢罐内存放7天时间</td>
              </tr>
              <tr>
                <td class='tdHeader'>外皮浸渍作用</td>
                <td>7 天</td>
              </tr>
             <tr>
                <td class='tdHeader'>乳酸发酵</td>
                <td>是</td>
              </tr>
              <tr>
                <td class='tdHeader'>成熟期</td>
                <td>在铁桶中存放7个月时间</td>
              </tr>
              <tr>
                <td class='tdHeader'>酒精度</td>
                <td>12.5% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>最小装瓶陈化时间</td>
                <td>三个月</td>
              </tr>
            </table>
            <b>品尝体验</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>外观</td>
                <td>红宝石色</td>
              </tr>
              <tr>
                <td class='tdHeader'>嗅觉感受</td>
                <td>花香和果香、完美融合。</td>
              </tr>
              <tr>
                <td class='tdHeader'>味觉感受</td>
                <td>口感柔和圆润、带有淡淡的丹宁酸、令人回味、总体口味完美融合。</td>
              </tr>
              <tr>
                <td class='tdHeader'>适宜搭配</td>
                <td>适宜与第一道菜、以及不同种类的沙拉搭配食用。</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>

