<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>源自Streda IGT产地</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>我们的葡萄酒</a> » Casanova
          </div>        
          <div class='post'>
            <h1>Casanova di Streda Belvedere</h1>
            <p>
              <h2>Supertuscan Toscana Rosso igp(igt)</h2>
            <p>
            <img src='images/wines/casanova.png' style='float:left;'/>
            <b>产地特征</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>生产商</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄种类：</td>
                <td>60%的桑娇维赛、30%的梅乐和10%的席拉。</td>
              </tr>              
              <tr>
                <td class='tdHeader'>海拔</td>
                <td>200 米</td>
              </tr>
              <tr>
                <td class='tdHeader'>方向和土壤面貌</td>
                <td>自东向西方向。中等土质、多粘土、含有钙化化石成分。</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄园密度</td>
                <td>4500 个葡萄藤/每公顷</td>
              </tr>
              <tr>
                <td class='tdHeader'>培育方法</td>
                <td>带刺的包围隔离</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄藤平均成熟时间</td>
                <td>15 年</td>
              </tr>
            </table>
            <b>发酵和成熟</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>每公顷的产量</td>
                <td>90公担</td>
              </tr>
              <tr>
                <td class='tdHeader'>收获期</td>
                <td>九月中旬、机器收割</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄加工</td>
                <td>压碎、去梗。</td>
              </tr>
              <tr>
                <td class='tdHeader'>发酵温度和时间</td>
                <td>在28摄氏度的不锈钢发酵罐内、存放7天时间</td>
              </tr>
              <tr>
                <td class='tdHeader'>外皮浸渍作用</td>
                <td>12 天</td>
              </tr>
             <tr>
                <td class='tdHeader'>乳酸发酵</td>
                <td>是</td>
              </tr>
              <tr>
                <td class='tdHeader'>成熟期</td>
                <td>70%存放在铁罐内、30%存放在法国橡木桶中、存放时间为六个月。 最后将两个存放在不同地方的葡萄酒混合、然后在铁罐中再存放几个月、最后装瓶。</td>
              </tr>
              <tr>
                <td class='tdHeader'>酒精度</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>最小装瓶陈化时间</td>
                <td>三个月</td>
              </tr>
            </table>
            <b>品尝体验</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>外观</td>
                <td>深红宝石色</td>
              </tr>
              <tr>
                <td class='tdHeader'>嗅觉感受</td>
                <td>浓郁的红色葡萄果味、香味、淡淡的香草香味</td>
              </tr>
              <tr>
                <td class='tdHeader'>味觉感受</td>
                <td>温暖、柔和、可口的丹宁酸在口中慢慢扩散、带有爽口的酸味、回味悠长、口感适宜。</td>
              </tr>
              <tr>
                <td class='tdHeader'>适宜搭配</td>
                <td>适宜与所有肉类共同食用、包括家禽类和牛肉、新鲜的奶酪和熟食等。</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
