<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Location</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
          <h1>未来计划</h1>
            <h2>Vinci</h2>
            <img src='images/vinci.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>从美丽的中世纪Vinci小镇启程，沿着Cont Guidi广场的小道一路向前，在著名的建筑物和帕拉迪诺雕塑面前驻足欣赏。参观建于十三世纪的著名的Santa Croce教堂，李奥纳多•文森特本人就在这所教堂接受洗礼仪式。走进李奥纳多博物馆参观，从一条条支柱中隐约可见一座城堡，顶部还有一座高塔。站在这座高塔之上，你可以感受托斯卡纳地区举世无双的如画美景。在青翠欲滴的橄榄树林间漫步30分钟，你就能到达李奥纳多的住所。这趟旅程能带你领略李奥纳多大师年轻时候所看到的美景和听到的乐声。</p>
			<div class="clear"></div>
            
            <h2>高尔夫</h2>
            <img src='images/golf.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>穿过省道正对着Streda Belvedere的就是Bellosgaurdo高尔夫俱乐部，这家俱乐部内具有9洞球场。高尔夫球爱好者可以尽情享用修剪整齐的球场。 </p>
			<div class="clear"></div>
            
            <h2>远足</h2>
            <img src='images/hiking.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>如果有爱好运动的游客喜欢用双脚去感受大地，这里也有许多适合徒步旅行的美景和步道，一路攀爬即可通往Montalbano的最高峰。在游客接待处可以购买地图，游客可以根据个人能力和时间限制规划路线。</p>
			<div class="clear"></div>
            
            <h2>切雷托</h2>
            <img src='images/cerreto.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>在Cerreto Guidi附近是梅迪奇家族的狩猎小屋，小屋建于文艺复兴时期。同样地，附近还有著名的李奥纳多教堂，教堂是在罗马废墟的基础上建造的。</p>
			<div class="clear"></div>          
          
            <h2>乘坐火车前往佛罗伦萨</h2>
            <img src='images/firenze.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>到了托斯卡纳，一定要去看看托斯卡纳的首都城市——佛罗伦萨。乘坐火车前往佛罗伦萨既方便又经济，因为在Empoli火车站每隔15-30分钟就有一辆前往佛罗伦萨的火车。只需3欧元，你就可以到达城市的中心。如果游客喜欢从乡间小道驾车前往，当地的日停车费用约3欧元。乘坐火车价格低廉，但条件相对简陋、不够舒适，但火车已经是前往这座首都城市最方便的交通方式了。</p>
			<div class="clear"></div>            

            <h2>蒙特卡蒂尼温泉</h2>
            <img src='images/montecatini.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>这所著名的温泉浴场近年进行了翻修，周围是一个巨大的景观花园和一个美景花园，令人心旷神怡。在舒适的温泉浴场中，在免费欣赏花园景色的时候，你可以释放所有的担忧和压力。如果感到饿了，乘上缆车来到美丽的蒙特卡蒂尼Piazzeta，你会找到许许多多的饭店和酒吧，其中一定有适合你的口味的。这里还有许多琳琅满目的商店，在周日也向游客开放。</p>
			<div class="clear"></div>

            <h2>Viareggio和Forte dei Marmi</h2>
            <img src='images/viareggio.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>距离Streda Belvedare 70公里处就是维西利亚海滩，游客可以一整天躺在温暖的沙滩上晒太阳。到了夏天，可以在周六或周日前来游玩，那时海滩上不会非常拥挤，游客也能在温暖的海水中更放松地享受这趟旅程。</p>
			<div class="clear"></div>

            <h2>皮斯托亚</h2>
            <img src='images/pistoia.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>距离Streda Belvedare 40分钟车程，皮斯托亚坐落在维赛兹奥山谷的另一面，是一个混合了现代和古老建筑的热闹的城市，四周围绕着古罗马城墙。在市中心，是意大利最大的哥特式大教堂——Baptisery，即主教的宫殿，还有一个小型广场，广场上售卖各式各样的水果和蔬菜。七月和八月里，每到星期三和星期六的早晨，这里就会有盛大的当地集市，售卖难以计数的独特商品。在其他月份，集市就在古老的Piazza del Duomo内进行。在皮斯托亚一直闲逛到傍晚也非常愉快，当地人纷纷出街，市中心到处都是享用传统美食和饮料的人们。如果天气条件允许，你也可以在室外就餐，享用你的休闲时光。</p>
			<div class="clear"></div>            

            <h2>比萨斜塔</h2>
            <img src='images/pisa.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>世界著名的比萨斜塔无需作过多介绍，凡是来到托斯卡纳游玩，游客都会去看一看这道风景。从Empoli乘坐火车就能到达，或者直接乘坐汽车经过Fl Pl Ll高速公路即可到达。</p>
			<div class="clear"></div>   

            <h2>锡耶纳</h2>
            <img src='images/Siena.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>前往锡耶纳，乘坐汽车需要2个小时，从Empoli乘坐火车仅需1个小时。如果是在度假期间，你可以乘坐火车直接前往目的地。</p>
			<div class="clear"></div>

            <h2>距离Streda Belvedere小于1小时车程的其他地方</h2>
            <ul>
              <li>
              切塔尔多，风景如画的古罗马托斯卡纳小镇</li>
              <li圣米尼亚托，有Frederick塔、教区博物馆和其他保护良好的中世纪景点。</li>
              <li>圣吉米纳诺，卢卡塔之镇，拥有100个教堂，距离Volterra——著名的伊德鲁里亚之城略超过1小时车程。</li>
            </ul>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
