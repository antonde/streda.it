<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Products</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>

      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarProd.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>我们的产品</h1>
            <p>
              我们提供橄榄油和家庭饮用的葡萄酒，能让托斯拉纳地区的家庭立即享用丰盛佳肴。<br/>
              按照地中海文化的习俗，托斯卡纳地区的伊特鲁利亚人和文艺复兴文化的创始人，当地人们认为美食的艺术离不开葡萄酒、橄榄油和葡萄。<br/>
              在餐桌上，葡萄酒和橄榄油相得益彰，成为了幸福的象征。<br/>
              如今，在托斯卡纳地区，葡萄酒和橄榄油已经成为了不可分割的历史和文化元素。<br/>
            <p>
            <div style='text-align:center;'>
              <a href='wine.php'><img class='nofloat' src='images/product1.jpg'/></a>
              <br/><br/>
              <a href='grappa.php'><img class='nofloat' src='images/product2.jpg'/></a>
              <br/><br/>
              <a href='oil.php'><img class='nofloat' src='images/product3.jpg'/></a>
			<br/><br/>
			<a href='http://blog.streda.it/category/award-review/'><img class='nofloat' src='images/product4.jpg'/></a>
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
