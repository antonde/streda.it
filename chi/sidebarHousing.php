<h1>Our Products</h1>
<img src='images/wineSmall.png' style='float:left;'/>
<p>
  Browse our range of wines, olive oils and grappa all made here at Streda Belvedere<br/>
  <a href='products.php'><img src='images/buttonBrowse.png'/></a>
</p>
<hr/>
<h1>Accomodations:</h1>
<p>
  There are 4 rooms and 13 one to two bedroom apartments.  Each are equipped with a kitchenette, located within 2 rural farmhouses.
</p>
<a href='accomodations.php'><img src='images/buttonComeStay.png'/></a>
<img src='images/sideimage1.jpg'/>
  <a href="http://pictures.streda.it" target="_new"><img src='images/gallery.png'/></a>
<hr/>
<h1>From our Blog:</h1>

<?php
        require_once('simplepie/simplepie.inc');
        $feed = new SimplePie();
$feed->set_feed_url(array('http://blog.streda.it/feed'));
        $feed->init();
        $feed->handle_content_type();
        $feed->set_item_limit(3);
?>


<?php
	foreach ($feed->get_items() as $item):
?>
	<div class="item">
        <h2><a href="<?php echo $item->get_permalink(); ?>"><?php echo $item->get_title(); ?></a></h2>
        <p><?php echo $item->get_description(); ?><a href="<?php echo $item->get_permalink(); ?>"> More</a></p>
        <!-- <p><small>Posted on <?php echo $item->get_date('j F Y | g:i a'); ?></small></p> -->
	</div>
<?php endforeach; ?>
