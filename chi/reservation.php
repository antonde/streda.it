<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Request Availability</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
	    $(function() {
		    $( "#arriving" ).datepicker();
		    $( "#departing" ).datepicker();
	    });
	  </script>
	<script type="text/javascript">
    $(document).ready(function() {
      $("#contact_form").validate({
        rules: {
          Name: "required",//name field validate
          Email: {// compound rule
	          required: true,
	          email: true
	        },
	      Country: {
		          required: true,
		        },
		  Arriving: {
	          required: true,
	        },
	      Departing: {
	          required: true,
	        },
	      NumberofAdults: {
		          required: true,
		        },
		  Numberofkidsunder13: {
			          required: true,
			        }
        },
        messages: {
            message: "Please enter a comment.",
  		  name:"Please enter your name."
  		 }
      });
    });

  </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
        	<?php 
        	if($_REQUEST['Email']){
	          	$to      = 'info@streda.it';  
				$subject = "Richiesta disponibilita'";
				$message = "<html>
							<head>
							</head>
							<body>
							  <p><b>Richiesta disponibilit&agrave;</b></p> 
							  <p>E-mail: ".$_REQUEST['Email']."</p> 
							  <p>Nome: ".$_REQUEST['Name']."</p> 
							  <p>Cognome: ".$_REQUEST['Surname']."</p> 
							  <p>Citt&agrave;: ".$_REQUEST['City']."</p> 
							  <p>CAP: ".$_REQUEST['CAP']."</p> 
							  <p>Stato: ".$_REQUEST['Country']."</p> 
							  <p>Telefono: ".$_REQUEST['PhoneNumber']."</p> 
							  <p>Arrivo: ".$_REQUEST['Arriving']."</p>
							  <p>Partenza: ".$_REQUEST['Departing']."</p>
							  <p>Numero di adulti: ".$_REQUEST['NumberofAdults']."</p> 
							  <p>Numero di bambini sotto i 13 anni: ".$_REQUEST['Numberofkidsunder13']."</p> 
							  <p>Appartamento: ".$_REQUEST['Unit']."</p> 
							  <p>Richieste speciali: ".$_REQUEST['SpecialRequests']."</p> 
							</body>
							</html> ";
				
				$headers = 'From: streda@streda.it' . "\n"; 
				$headers .= 'MIME-Version: 1.0' . "\n"; 
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n"; 
				
				mail($to, $subject, $message, $headers);
				
				echo '<p>Your email has been successfully sent.  We will get back to you soon</p>';
          	}
          	?>
          <div class='post'>
            <h1>Request Availability</h1>
            <form name='form' id="contact_form" method="post">
              <table class='form'>
                <tr>
                  <td>*电子邮件*</td>
                  <td><input type='text' name='Email'/></td>
                </tr>
                <tr>
                  <td>*姓名*</td>
                  <td><input type='text' name='Name'/></td>
                </tr>
                <tr>
                  <td>*姓名*</td>
                  <td><input type='text' name='Surname'/></td>
                </tr>
                <tr>
                  <td>城市</td>
                  <td><input type='text' name='City'/> CAP: <input type='text' name='CAP'/></td>
                </tr>
                <tr>
                  <td>*国家*</td>
                  <td><input type='text' name='Country'/></td>
                </tr>
                <tr>
                  <td>联系电话</td>
                  <td><input type='text' name='PhoneNumber'/></td>
                </tr>
                <tr>
                  <td>*时间*</td>
                  <td>
                    从 <input type="text" name="Arriving" id='arriving'/>
                    至 <input type='text' name='Departing' id='departing'/>
                  </td>
                </tr>
                <tr>
                  <td>*成人数量*</td>
                  <td><input type='text' name='NumberofAdults'/></td>
                </tr>
                   <tr>
                  <td>*13 岁以下儿童数量*</td>
                  <td><input type='text' name='Numberofkidsunder13'/></td>
                </tr>
               
                <?php
                  if(isset($_GET["unit"])){
                    echo "<tr><td>Unit Requested</td><td>";
                    echo "<input type='text' name='Unit' value='".$_GET["unit"]."'/>";
                    echo "</td></tr>";
                  }
                ?>
                <tr>
                  <td>特殊要求</td>
                  <td><textarea name='SpecialRequests' style='width:90%;min-height:120px;'></textarea></td>
                </tr>
              </table>
              <div style='text-align:center;'>
                <input type='submit' value='Send' id='submit'/>
              </div>
 			<h2>联系方式</h2>
              <p>
            Streda Belvedere Azienda Agricola</br>
			电话+39 0571 729195 传真+39 0571 568563 电子邮件)</br>
			E-mail streda@streda.it</br>
            </p>
              </p>
            </form>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
