<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Contact</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
     <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
     <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    
<?php require_once('meta.php'); ?>
	<script>
	    $(function() {
		    $( "#arriving" ).datepicker();
		    $( "#departing" ).datepicker();
	    });
	</script>
	<script type="text/javascript">
    $(document).ready(function() {
      $("#contact_form").validate({
        rules: {
          Name: "required",//name field validate
          Email: {// compound rule
	          required: true,
	          email: true
	        },
	      PhoneNumber: {
		          required: true,
		        },
          message: {
	          required: true
	        },
		        Arriving: {
			          required: true
		        },
		        Departing: {
			          required: true
		        }
        },
        messages: {
            message: "Please enter a comment.",
  		  name:"Please enter your name."
  		 }
      });
    });

  </script>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
          	<h2>General Inquires</h2><br>
          	<?php 
          	if($_REQUEST['Email']){
	          	$to      = 'info@streda.it';  
				$subject = 'Richiesta generica';
				$message = "<html>
							<head>
							</head>
							<body>
							  <p><b>Richiesta generica</b></p>
							  <p>Email: ".$_REQUEST['Email']."</p> 
							  <p>Name: ".$_REQUEST['Name']."</p> 
							  <p>Surname: ".$_REQUEST['Surname']."</p> 
							  <p>City: ".$_REQUEST['City']."</p> 
							  <p>CAP: ".$_REQUEST['CAP']."</p> 
							  <p>Country: ".$_REQUEST['Country']."</p> 
							  <p>Phone Number: ".$_REQUEST['PhoneNumber']."</p> 
							  <p>FAX Number: ".$_REQUEST['FAXNumber']."</p> 
							  <p>Arriving: ".$_REQUEST['Departing']."</p> 
							  <p>Number of Adults: ".$_REQUEST['NumberofAdults']."</p> 
							  <p>Number of kids under 13: ".$_REQUEST['Numberofkidsunder13']."</p> 
							  <p>Unit: ".$_REQUEST['Unit']."</p> 
							  <p>Special Requests: ".$_REQUEST['SpecialRequests']."</p> 
							</body>
							</html> ";
				
				$headers = 'From: webmaster@example.com' . "\r\n" .
						   'Reply-To: webmaster@example.com' . "\r\n" .
						   'X-Mailer: PHP/' . phpversion();
				
				mail($to, $subject, $message, $headers);
				
				echo '<p>Your email has been successfully sent.  We will get back to you soon</p>';
          	}
          	?>
          	
          	 <h1>联系方式</h1>
              <p>
            Streda Belvedere Azienda Agricola</br>
			电话+39 0571 729195 传真+39 0571 568563 电子邮件)</br>
			
			E-mail streda@streda.it</br>
            </p>
            <h1>要求有效性</h1>
                        
            <form name='form' id="contact_form" method="post">
            	<table class='form'>
                <tr>
                  <td>*电子邮件*</td>
                  <td><input type='text' name='Email'/></td>
                </tr>
                <tr>
                  <td>*姓名*</td>
                  <td><input type='text' name='Name'/></td>
                </tr>
                <tr>
                  <td>姓氏</td>
                  <td><input type='text' name='Surname'/></td>
                </tr>
                <tr>
                  <td>城市</td>
                  <td><input type='text' name='City'/> CAP: <input type='text' name='CAP'/></td>
                </tr>
                <tr>
                  <td>国家</td>
                  <td><input type='text' name='Country'/></td>
                </tr>
                <tr>
                  <td>*电话号码*</td>
                  <td><input type='text' name='PhoneNumber'/></td>
                </tr>
                <tr>
                  <td>移动电话/手机号码</td>
                  <td><input type='text' name='FAXNumber'/></td>
                </tr>
                <tr>
                  <td>*时间*</td>
                  <td>
                    从 <input type="text" name="Arriving" id='arriving'/>
                    至 <input type='text' name='Departing' id='departing'/>
                  </td>
                </tr>
                <tr>
                  <td>成人数量：</td>
                  <td><input type='text' name='NumberofAdults'/></td>
                </tr>
                <tr>
                  <td>13岁以下儿童数量</td>
                  <td><input type='text' name='Numberofkidsunder13'/></td>
                </tr>
                <tr>
                  <td>房间数量</td>
                  <td><input type='text' name='Unit' value='<?=$_GET["unit"];?>'/></td>
                </tr>
                <tr>
                  <td>特殊要求</td>
                  <td><textarea name='SpecialRequests' style='width:90%;min-height:120px;'></textarea></td>
                </tr>
                <tr>
                  <td style='text-align:center;'><input type='checkbox' onclick='document.getElementById("submit").disabled=false'/> * </td>
                  <td>同意提交个人信息</td>
                </tr>
              </table>
              <p>
                注意：同意才可继续查看网站内容。同意提交表格即表示了解意大利法律及其保障的权利。
              <p>
              <div style='text-align:center;'>
                <input type='submit' value='Submit' id='submit' disabled='disabled'/>
              </div>
            </form>
            *必填内容<br>
          
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
