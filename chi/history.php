<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - History</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post' style='width:95%;'>
            <h1>VINCI</h1> <h2>数世纪以来，Vinci地区以生产橄榄油和葡萄酒闻名</h2>
            <p>
             坐落在托斯卡纳的中心地区，这座小镇以莱昂纳多（1452年4月15日）的故乡而闻名，在当地山谷连绵的美景中，Guidi城堡独树一帜，成为了当地的标志。</p>


<img src='images/vinci1.jpg'/>
<p>1953年起，中世纪的莱昂纳多博物馆坐落在Vinci的这座城堡之内，这座城堡在1940年重建，1986年和2004年经过翻修。博物馆内也陈列了受到莱昂纳多的画作和素描启发而制造的机器设备。 来自Giovanni della Robbia工作室的华丽陶瓷作品也在此展出。城堡毗连Palazzina Uzielli和莱昂纳多图书馆，馆内也珍藏着达芬奇的重要史证。自1993年起，坐落在地下画廊和古老酒窖内幽闭处的莱昂纳多•达芬奇的理想博物馆对公众开放，勾起人们的无限回忆。这座博物馆也是世界上第一座通过呈现莱昂纳多的古老原作（莱昂纳多工作室内的绘画、雕刻和考古发现），呈现与其传记、出生地相关的历史内容，向世界展现莱昂纳多身为一名艺术家、科学家、发明家和设计师的非凡魅力。</p>
<p>近期，按照莱昂纳多的项目（可操作项目和可行的项目）和莱昂纳多主义的奇迹（根据莱昂纳多时代与现代艺术家相关的大事件，包括 Marcel Duchamp），采用精良技术再现了超过六十多件模型。除此之外，理想博物馆还在附近建造了博物馆的室外延伸部分，称其为"莱昂纳多花园和乌托邦"的主题公园。在城堡的城墙之内，我们可以看到中世纪的圣十字架教堂，这座教堂在20世纪前期以新文艺复兴风格重建，也是1452年四月五日莱昂纳多接受洗礼的地方。此外，还有莱昂纳多诞辰第五百周年——1952年重建的洗礼池，以及一些17世纪和18世纪的画作。在Saint John第23大道的尽头，我们能够欣赏到Saint Annunziata的神龛，建于17世纪，带有一张巨大的Fra' Paolino da Pistoia祭坛：十六世纪的"报喜节"。在山坡上的 Anchiano小村庄——距离Vinci小镇两千米处，有一间石砌的小农舍（于1952年和1986年重建），这间小屋被认为是莱昂纳多出生的地方。在Sant'Ansano（Vinci附近），我们会看到Saint John（Greti的浸信会教友）的罗马式教堂，里面雕刻着中世纪的字母，还保留着Rutilio Manetti大师的杰作，Rutilio Manetti是17世纪生于锡耶纳的艺术大师。</p>
            </p>
            <h1>Streda</h1> <h2>Cerreto Guidi和Vinci中间的道路</h2>
            
              <img src='images/history1.jpg'/>
              <p>莱昂纳多应当值得拥有一张Streda和Vincio河流中间地区的最古老的地图。早在历史记载之前，这里就有人类居住，但罗马人是最早在Streda山谷留下人类文明的痕迹。</p> 

<p>1000年之后， San Bartolomeo教堂在Streda建立，是当地的罗马式教堂之一，当时属于 Lucca主教，现在属于San Miniato，被视为Pistoia主教区和Florence主教区的分界线。</p> </br></br></br></br></br></br>

</br></br></br>
<img src='images/2streda.jpg'/> 

<p>Guidi家族一直掌握着Streda地区的居民群和当地的两家工厂，直到1254年，权力才移交到了佛罗伦萨市政府的手中。提起1382年Vinci市政府的地位上，San Bartolomeo的重要性不言而喻。存放公职人员候选人名单的箱子有四把钥匙，其中一把就是由教堂保管的。我们从一张16世纪 Guelph Captains的地图上，就可以看到Streda地区为数不多但颇有意味的街道和当时 Santa Maria人民的建筑物，其中一些地产在15世纪属于莱昂纳多的父亲。</p>
            <br clear='both'/>
            
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
