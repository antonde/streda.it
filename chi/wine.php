<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda的葡萄酒</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>Streda的葡萄酒</h1>
            <p>
              早在12世纪和13世纪，Streda地区已经开始生产葡萄酒。在15世纪， Antonio da Vinci – 莱昂纳多•达芬奇的父亲就向土地管理处提交报告，说明他在Streda河流沿岸和Fossato di Vinci附近的自家土地上生产了十六桶葡萄酒。如今，Streda继承了酿造葡萄酒的悠久传统，不断生成出托斯卡纳地区独特的葡萄酒。
            <p>
            <div style='text-align:center;'>
              <img class='nofloat' src='images/wine1.jpg'/>
            </div>
            <h1>我们的红葡萄酒<h1>
            <div style='text-align:center;'>
              <a href='wine_chianti.php'><img class='nofloat' src='images/wine2.png'/></a>
              <a href='wine_sole.php'><img class='nofloat' src='images/wine_sole1.png'/></a>
              <a href='wine_brunello.php'><img class='nofloat' src='images/wine6.png'/></a>
              <a href='wine_casanova.php'><img class='nofloat' src='images/wine4.png'/></a>
              <a href='wine_toiano.php'><img class='nofloat' src='images/wine3.png'/></a>
              <a href='wine_syrah.php'><img class='nofloat' src='images/wine5.png'/></a>
              <br/>
            </div>
            <h1>我们的白葡萄酒</h1>
            <div style='text-align:center;'>
              <a href='wine_rosso.php'><img class='nofloat' src='images/wine7.png'/></a>
              <a href='wine_bianco.php'><img class='nofloat' src='images/wine8.png'/></a>
              <a href='wine_chardonnay.php'><img class='nofloat' src='images/wine9.png'/></a>
              <a href='wine_duccio_rose.php'><img class='nofloat' src='images/duccio_rose.png'/></a>
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
