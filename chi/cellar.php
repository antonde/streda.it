<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - 我们的葡萄酒窖</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>Our Wine Cellars</h1>
            <div style='text-align:center;'>
              <img class='nofloat' src='images/cellar1.jpg'/>
            </div>
            <br/>
            葡萄酒的发酵过程在控制温度的铁罐内进行，当葡萄酒在由法国橡木密封的小桶内完成陈化作用之后，把酒装入玻璃容器，以供最终处理。
            <br/>
            <div style='text-align:center;'>
              <img class='nofloat' src='images/cellar2.jpg'/>
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
