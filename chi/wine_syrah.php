<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Syrah</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>我们的葡萄酒</a> » Syrah
          </div>        
          <div class='post'>
            <h1>Drufo Syrah di Streda Belvedere</h1>
            <p>
              <h2>Toscana rosso igp(igt)</h2>
            <p>
            <img src='images/wines/syrah.png' style='float:left;'/>
            <b>产地特征 </b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>生产商</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄种类：</td>
                <td>Syrah</td>
              </tr>
              <tr>
                <td class='tdHeader'>海拔</td>
                <td>200 米</td>
              </tr>
              <tr>
                <td class='tdHeader'>方向和土壤面貌</td>
                <td>自东向西方向。中等土质、带有钙化化石成分。</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄园密度</td>
                <td>4500 个葡萄藤/每公顷</td>
              </tr>
              <tr>
                <td class='tdHeader'>培育方法</td>
                <td>带刺的包围隔离</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄藤平均成熟时间</td>
                <td>15 年</td>
              </tr>
            </table>
            <b>发酵和成熟</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>每公顷的产量</td>
                <td>70 公担</td>
              </tr>
              <tr>
                <td class='tdHeader'>收获期</td>
                <td>九月中旬、机器收割</td>
              </tr>
              <tr>
                <td class='tdHeader'>葡萄加工</td>
                <td>压碎、去梗。</td>
              </tr>
              <tr>
                <td class='tdHeader'>发酵温度和时间</td>
                <td>在32摄氏度的不锈钢罐内存放7天时间</td>
              </tr>
              <tr>
                <td class='tdHeader'>外皮浸渍作用</td>
                <td>20 天</td>
              </tr>
             <tr>
                <td class='tdHeader'>乳酸发酵</td>
                <td>是</td>
              </tr>
              <tr>
                <td class='tdHeader'>成熟期</td>
                <td>在法国橡木桶内存放12个月</td>
              </tr>
              <tr>
                <td class='tdHeader'>酒精度</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>最小装瓶陈化时间</td>
                <td>6 个月</td>
              </tr>
            </table>
            <b>品尝体验</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>外观</td>
                <td>深红宝石色</td>
              </tr>
              <tr>
                <td class='tdHeader'>嗅觉感受</td>
                <td>完美混合的花香和果香、带有香草的芬芳</td>
              </tr>
              <tr>
                <td class='tdHeader'>味觉感受</td>
                <td>入口即化、优雅格调、伴随隐约的橡木香气、浓郁柔和的丹宁酸令人久久回味。</td>
              </tr>
              <tr>
                <td class='tdHeader'>适宜搭配</td>
                <td>比赛、烧烤时饮用、或与成熟干酪搭配食用。</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
