<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Casanova</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Our Wine</a> » Casanova
          </div>        
          <div class='post'>
            <h1>Casanova di Streda Belvedere</h1>
            <p>
              <h2>Supertuscan Toscana Rosso igp(igt)</h2>
            <p>
            <img src='images/wines/casanova.png' style='float:left;'/>
            <b>Charateristics of production area</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Producer</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape Type:</td>
                <td>Sangiovese 60%, Merlot 30% and 10% Syrah </td>
              </tr>              
              <tr>
                <td class='tdHeader'>Altitude</td>
                <td>200 metres</td>
              </tr>
              <tr>
                <td class='tdHeader'>Aspect and soil profile</td>
                <td>East-west. Medium textured, largely clay with calcareous fossil elements</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vineyard density</td>
                <td>4500 vines/hectare</td>
              </tr>
              <tr>
                <td class='tdHeader'>Training method</td>
                <td>Spurred cordon</td>
              </tr>
              <tr>
                <td class='tdHeader'>Average vine age</td>
                <td>15 years</td>
              </tr>
            </table>
            <b>Fermentation and Maturation</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Yield per hectare </td>
                <td>90 quintals of grape</td>
              </tr>
              <tr>
                <td class='tdHeader'>Harvest period</td>
                <td>Mid- September, mechanically harvested</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape processing</td>
                <td>Crusher- destemmer </td>
              </tr>
              <tr>
                <td class='tdHeader'>Temp and length of fermentation</td>
                <td>28°C per 7 days in stainless steel</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maceration on skins</td>
                <td>12 days</td>
              </tr>
             <tr>
                <td class='tdHeader'>Malolactic fermentation</td>
                <td>Yes</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maturation</td>
                <td> the variety of the grape are fermented separately after a short time that the fermentation is concluded the variety are assembled togheter to form the Casanova Blend 30% of the blend is put in a soak for a few months in French oak barrels until it is ready to be blended with the remaining 70% which has continued its maturation in steel. Once the two are blended, they stay together for several more months before bottling</td>
              </tr>
              <tr>
                <td class='tdHeader'>Alcohol</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Minimun bottle ageing</td>
                <td>3 months</td>
              </tr>
            </table>
            <b>Tasting notes</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Appearance</td>
                <td>Deep ruby</td>
              </tr>
              <tr>
                <td class='tdHeader'>Nose</td>
                <td>Wild red berry fruit, spices, and subtle notes of vanilla</td>
              </tr>
              <tr>
                <td class='tdHeader'>Palate</td>
                <td>Warm, supple, and expansive, with velvety tannins, and with a crisp acidity; lengthy finish that completes a very harmonious wine</td>
              </tr>
              <tr>
                <td class='tdHeader'>Serving suggestions</td>
                <td>All meats, even poultry and veal, young cheeses and charcuterie</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
