<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Olive Oil</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebarFarm.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
            <h1>TUSCAN IGP – EXTRA VERGIN OLIVE OIL</h1>
            <img src='images/oil1.png'/>
            <p>
              The STREDA BELVEDERE extra-vergin olive oil IGP is the result of pure tradition.
              This oil is made for those who love the typical fragrances of Tuscany , produced in limited quantities from the most known and typical variety that grows on the Montalbano hills like olives moraiole, leccine and frantoiane.
              Due to the presence of poliphenols and all the special property since ever this olive oil it is has been used for feeding the children.
              It is also used as a dressing for salad and raw vegetables and for cooking the most famous Tuscan recipes it is also the main ingredient as topping for the famous “bruschetta “ , and it is also largely used in the skin care and other well-ness treatment.
              The unique of the olive oil igp streda belvedere that grows in our land in vinci Toscana comes from different factors the unique kind of ground and his geological composition, of course the climate and a tradition of centuries of growing and selecting olives trees All this gives an unmistakable and remarkable organoleptic characteristic with a color that goes from green to yellow gold with a chromatic variation with the time with a smell of almond ripe fruit.
              The IGP certification state that all the process of production ,from the growing ,the harvesting of the olives,the pressing and the packaging are esclusively made in Tuscany and all the characteristic chemichal-phisical and organoleptic of the product respect the statement of the consortium of IGP Tuscan olive oil
              <br/></br>
              <img src='images/oil2.png' style='float:right;'/>
              <b>Bottles</b><br/>
              Da 0,750 LT<br/>
              Da 0,500 LT<br/>
              Da 0,250 LT<br/>
              <br/>
              <b>Cans</b><br/>
              Da 5 LT
            <p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
