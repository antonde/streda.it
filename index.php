<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Home</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/image1.jpg',
          'images/image2.jpg',
          'images/image3.jpg',
          'images/image4.jpg',
          'images/image5.jpg',
          'images/image6.jpg',
        ]);
      });
      imgDir = "";
    </script>
<?php require_once('meta.php'); ?>


 </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='content'>
        <div class='imagePreview'>
          <img id='oldImage' src='images/image1.jpg'/>
          <img id='newImage' src='images/image1.jpg' class='previewLoader'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/image6.png' onclick='changePreview(6)' /></li>
              </ul>
            </div>
          </div>
        </div>
        <a href='accomodations.php'><img src='images/buttonFancy.png'/></a>
        <div class='post'>
          <h1>A Truly Romantic Italian Borgo</h1>
          <p>
            Streda Belvedere is located in Vinci, the birthplace of celebrated Maestro Leonardo da Vinci, in the heart of Tuscany, not far from Florence, Pisa, San Gemignano and Siena.
            The village grew up in romanequeue era, around the church of San Bartolomeo a Streda, whose earliest remains date back to about the year 1000.
          </p>
          <img src='images/article01.png'/>
          <p>
            The romantic borgo has been reconstructed in recent years and offers an unparalleled opportunity to spend a vacation liersurely immeresed in history, vines and olive trees that have been cultivated for many generations. Ser Pieno, Leonardo DaVinci's father, was growing vines in Streda in the 16th century.
          </p>
          <a href='history.php'>Click here to learn about Streda Belvedere</a>, or <a href='accomodations.php'>click here for Accomodations</a>.
        </div>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
