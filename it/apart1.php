<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Merlot</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/image1.jpg',
          'images/image2.jpg',
          'images/image3.jpg',
          'images/image4.jpg',
          'images/image5.jpg',
          'images/image6.jpg',
        ]);
      });
      imgDir = "merlot";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <h1>Our Products</h1>
          <a href='products.php'><img src='images/wineSmall.png' style='float:left;'/></a>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque auctor cursus pellentesqu.<br/>
            <img src='images/buttonBrowse.png'/>
          </p>
          <hr/>
          <h1>Accomodations:</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque auctor cursus. Pellentesque lacus risus, scelerisque in imperdiet malesuada.
          </p>
          <img src='images/buttonComeStay.png'/>
          <div class='thumb'><img src='images/merlot/image1.png'/></div>
          <div class='thumb'><img src='images/merlot/image2.png'/></div>
          <div class='thumb'><img src='images/merlot/image3.png'/></div>
          <div class='thumb'><img src='images/merlot/image3.png'/></div>
          <div class='thumb'><img src='images/merlot/image2.png'/></div>
          <div class='thumb'><img src='images/merlot/image1.png'/></div>
          <hr/>
          <h1>From our Blog:</h1>
          <h2>Lorem ipsum dolar sit amet</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque auctor cursus. Pellentesque lacus risus, spulvinar neque. Suspen cidunt quis iaculis quis... [ <a>More</a> ]</p>
          <h2>Lorem ipsum dolar sit amet</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque auctor cursus. Pellentesque lacus risus, spulvinar neque. Suspen cidunt quis iaculis quis... [ <a>More</a> ]</p>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php  '>Accomodations</a> » Merlot
          </div>
          <h1>Apartment Name</h1>
        <div class='imagePreview'>
          <img id='oldImage' src='images/preview1.jpg'/>
          <img id='newImage' src='images/preview2.jpg' class='previewLoader'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/merlot/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/merlot/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/merlot/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/merlot/image1.png' onclick='changePreview(4)' /></li>
                <li><img src='images/merlot/image2.png' onclick='changePreview(5)' /></li>
                <li><img src='images/merlot/image3.png' onclick='changePreview(6)' /></li>
                <li><img src='images/merlot/image1.png' onclick='changePreview(7)' /></li>
              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>Description:</h2>
            <p>
              Since 1953 the medieval Leonardo Museum in Vinci has been located in this castle, which was restored in 1940 and renovated in 1986 and 2004.
              Models of machines inspired by the drawings and sketches of Leonardo are exhibited here.
              Also on display is a magnificent ceramic from the Giovanni della Robbia workshop.
            </p>
            <h2>Unit Details:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Size (in square meters) :</td>
                <td>500</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of rooms :</td>
                <td>6</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of beds :</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of washrooms :</td>
                <td>1.5</td>
              </tr>
              <tr>
                <td class='tdHeader'>Kitchen (full / half / none) :</td>
                <td>Full</td>
              </tr>
              <tr>
                <td class='tdHeader'>Air Conditioning :</td>
                <td>Yes</td>
              </tr>
              <tr>
                <td class='tdHeader'>Location (Main Floor / Second Floor) :</td>
                <td>Main Floor</td>
              </tr>
            </table>
            <a><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <hr/>
        © Copyright 2011 Streda Belvedere
        <div style='float:right;'>
          <a>Home</a> <span style='padding:8px;'>|</span>
          <a>Our Farm</a> <span style='padding:8px;'>|</span>
          <a>Accommodations</a> <span style='padding:8px;'>|</span>
          <a>Blog</a> <span style='padding:8px;'>|</span>
          <a>Contact</a>
        </div>
        <br clear='both'/>
        <div style='float:right;font-size:smaller;'>
          Azienda Agricola Streda 50059 Streda-Vinci (FI) P.Iva 05426360482<br/>
          Tel+39 0571 729195 Fax +39 0571 568563 E-mail streda@streda.it
        </div>
      </div>
    </div>
  </body>
</html>
