<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Toiano Merlot</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Nostro Vino</a> » Toiano Merlot
          </div>
          <div class='post'>
            <h1>Toiano Merlot di Streda Belvedere</h1>
            <p>
              <h2>Toscana rosso igp(igt)</h2>
            <p>
            <img src='images/wines/merlot.png' style='float:left;'/>
            <b>Caratteristiche territorio di produzione</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Produttore</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tipo</td>
                <td>Merlot</td>
              </tr>
              <tr>
                <td class='tdHeader'>Altimetria</td>
                <td>200 metri</td>
              </tr>
              <tr>
                <td class='tdHeader'>Esposizione e tipologia suolo</td>
                <td>Est-Ovest. Medio impasto tendenzialmente argilloso con fossili calcarei</td>
              </tr>
              <tr>
                <td class='tdHeader'>Densit&agrave; impianti</td>
                <td>5500 ceppi/ha</td>
              </tr>
              <tr>
                <td class='tdHeader'>Sistema allevamento</td>
                <td>Cordone speronato</td>
              </tr>
              <tr>
                <td class='tdHeader'>Et&agrave; media vigneto</td>
                <td>10 anni</td>
              </tr>
            </table>
            <b>Vinificazione e affinamento</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Resa ha </td>
                <td>80 quintali di uva</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vendemmia</td>
                <td>Met&agrave; settembre, meccanica</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pigiatura delle uve</td>
                <td>Con diraspatrice </td>
              </tr>
              <tr>
                <td class='tdHeader'>Temperatura e durata della fermentazione</td>
                <td>28&deg;C per 7-10 giorni in acciaio inox</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tempi macerazione sulle bucce</td>
                <td>12 giorni</td>
              </tr>
             <tr>
                <td class='tdHeader'>Fermentazione malolattica</td>
                <td>Si</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento</td>
                <td>Circa l’80% del vino matura in vasche di acciao e soltanto il 20% piccole botti di rovere francese(la permanenza nel legno varia secondo le annate).I due lotti di vino vengo nuovamente riuniti in vasche di acciaio alcuni mesi prima dell’imbottigliamento .</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grado alcolico</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento minimo in bottiglia</td>
                <td>6 mesi</td>
              </tr>
            </table>
            <b>Note di degustazione</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Colore</td>
                <td>Rosso rubino con riflessi violacei</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profumi</td>
                <td>Note intense di frutti neri e sottobosco con leggeri sentori di vaniglia</td>
              </tr>
              <tr>
                <td class='tdHeader'>Gusto</td>
                <td>Equilibrato, con tannini morbidi e ricco retrogusto</td>
              </tr>
              <tr>
                <td class='tdHeader'>Abbinamenti</td>
                <td>Formaggi freschi e primi piatti</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
