<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Location</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
          <h1>Location</h1>
            <iframe width="100%" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=streda+belvedere&amp;aq=&amp;sll=43.779852,10.923444&amp;sspn=0.209954,0.528374&amp;g=Vinci+Florence,+Italy&amp;ie=UTF8&amp;t=m&amp;cid=7639094987324374064&amp;hq=streda+belvedere&amp;hnear=&amp;ll=44.042193,10.931396&amp;spn=0.947673,1.757813&amp;z=9&amp;iwloc=A&amp;output=embed"></iframe>
            <br/>
            Visualizzazione ingrandita della mappa
            </br>
            <h2>In Treno</h2>
            La stazione ferroviaria più vicina a Streda Belvedere è la Stazione di Empoli raggiungibile in 10 minuti via Taxi.<br/>
            Con il treno si raggiungono agevolmente anche le seguenti destinazioni: Firenze, Pisa, e Siena.<br/>
            <a href='http://www.trenitalia.com'target='_blank'>Clicca qui.</a>
            <h2>In Aereo</h2>
            <p>
              <a href='http://www.aeroporto.firenze.it/'target='_blank'>Firenze Peretola</a><br/>
              Dista circa 40 minuti in auto da Streda Belvedere
            </p>
            <p>
              <a href='http://www.pisa-airport.com/'target='_blank'>Pisa Galileo Galilei</a><br/>
              Dista circa 50 minuti in auto da Streda Belvedere
            </p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
