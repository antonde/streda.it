<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Products</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>

      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarProd.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>I nostri prodotti</h1>
            <p>
Olio e vino di casa ed è subito festa sulla tavola toscana.
Questa è la regola, in tutte le antiche civiltà mediterranee; ma nella terra dove sono passati gli Etruschi e gli artefici del Rinascimento, qualcosa ha legato ancora di più il discorso dell’olio e del vino, dell’ulivo e della vite, alla civilta’ della cucina.
Uno scambio, una simbiosi tra questi due strumenti di felicità della mensa.
In Toscana, il Vino e l’olio sono parte ormai inscindibile della nostra storia e della nostra cultura           
            <p>
            <div style='text-align:center;'>
              <a href='wine.php'><img class='nofloat' src='images/product1.jpg'/></a>
              <br/><br/>
              <a href='grappa.php'><img class='nofloat' src='images/product2.jpg'/></a>
              <br/><br/>
              <a href='oil.php'><img class='nofloat' src='images/product3.jpg'/></a>
			<br/><br/>
			<a href='http://blog.streda.it/category/award-review/'><img class='nofloat' src='images/product4.jpg'/></a>

            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
