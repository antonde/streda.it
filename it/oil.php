<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Olive Oil</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebarFarm.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
            <h1>Olio Extra Vergine di Oliva IGP TOSCANO</h1>
            <img src='images/oil1.png'/>
            <p>
              L&rsquo;olio extra vergine IGP Streda Belvedere Vinci-Toscana &egrave; il frutto della pura tradizione.<br />
              Un olio per chi ama le fragranze tipiche, prodotto in quantit&agrave; limitata, nasce dalle olive 
              moraiole, leccine e frantoiane tipiche dei colli del Montalbano, colte e subito molite per non perderne i profumi e  sapori.<br />
              Una antica tradizione salutare dovuta anche alla presenza di polifenoli che rendono l&rsquo;olio particolarmente adatto per l&rsquo;alimentazione dei bambini, per le specialit&agrave; gastronomiche pi&ugrave;       caratteristiche e rinomate, e perfino  nelle cure estetiche.              <br />
              La tipicit&agrave;  dell&rsquo;Olio IGP Streda Belvedere Vinci-Toscana deriva da un&rsquo;insieme di fattori  tra i quali l&rsquo;unicit&agrave; della terra d&rsquo;origine, il clima e la particolare composizione geologica del terreno. Nasce cos&igrave; un olio dalle caratteristiche organolettiche unico e inimitabile per origine e tipicit&agrave;  con un colore dal verde al giallo, con variazione cromatica nel tempo, un odore fruttato accompagnato da un sentore di mandorla e frutta matura.<br />
        La certificazione IGP attesta che tutte le fasi produttive, dalla coltivazione e raccolta delle 
        olive alla loro frangitura fino all&rsquo;imbottigliamento si svolgono esclusivamente nel nostro <br />
        territorio in Toscana e che le qualit&agrave;  chimico-fisiche ed organolettiche del prodotto
      rispettano i parametri previsti dal rigoroso disciplinare di produzione.<br /></br>
              <img src='images/oil2.png' style='float:right;'/>
              <b>Bottiglie</b><br/>
              Da 0,750 LT<br/>
              Da 0,500 LT<br/>
              Da 0,250 LT<br/>
              <br/>
              <b>Lattine</b><br/>
              Da 5 LT
            <p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
