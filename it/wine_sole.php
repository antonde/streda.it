<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Sole Chianti Superiore DOCG</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Nostro Vino</a> » Sole Chianti Superiore DOCG
          </div>        
          <div class='post'>
            <h1>Sole di Streda Belvedere</h1>
            <p>
              <h2>Chianti superiore dop(docg)</h2>
            <p>
            <img src='images/wines/wine_sole.png' style='float:left;'/>
            <b>Caratteristiche territorio di produzione</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Produttore</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tipo</td>
                <td>90% Sangiovese - 10% altri vitigni consentiti dal DOCG</td>
              </tr>
              <tr>
                <td class='tdHeader'>Altimetria</td>
                <td>200 metri</td>
              </tr>
              <tr>
                <td class='tdHeader'>Esposizione e tipologia suolo</td>
                <td>Est-Ovest. Medio Impasto, Principalmente argilloso con elementi calcarei fossili</td>
              </tr>
              <tr>
                <td class='tdHeader'>Densità impianti</td>
                <td>4500 ceppi/ha</td>
              </tr>
              <tr>
                <td class='tdHeader'>Sistema aleevamento</td>
                <td>Cordone Speronato</td>
              </tr>
              <tr>
                <td class='tdHeader'>Età media vigneto</td>
                <td>15 anni</td>
              </tr>
            </table>
            <b>Vinificazione e affinamento</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Resa ha</td>
                <td>90 quintali di uva</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vendemmia</td>
                <td>Metà Settembre, meccanica</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pigiatura delle uve</td>
                <td>Con diraspatrice</td>
              </tr>
              <tr>
                <td class='tdHeader'>Temperatura e durata della fermentazione</td>
                <td>28°C per 20 giorni in botti d'acciaio</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tempi macerazione sulle bucce</td>
                <td>12 giorni</td>
              </tr>
             <tr>
                <td class='tdHeader'>Fermentazione malolattica</td>
                <td>Sì</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento</td>
                <td>8 mesi in botti di cemento</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grado alcolico</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento minimo in bottiglia</td>
                <td>3 months</td>
              </tr>
            </table>
            <b>Note di degustazione</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Colore</td>
                <td>Rosso rubino</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profumi</td>
                <td>Frutti rossi, fiori di violetta, molto intenso.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Gusto</td>
                <td>Molto equilibrato, con note di tannino dolce e buona acidità, elegante e succoso.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Abbinamenti</td>
                <td>Ottimo per antipasti di terra e carni, ma anche con insalate e formaggi a media stagionatura.</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>

