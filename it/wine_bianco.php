<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Duccio Bianco</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Nostro Vino</a> » >Duccio Bianco
          </div>
          <div class='post'>
            <h1>Duccio di Streda Belvedere Bianco</h1>
            <p>
              <h2>Toscana Bianco igp(igt)</h2>
            <p>
            <img src='images/wines/bianco.png' style='float:left;'/>
            <b>Caratteristiche territorio di produzione</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Produttore</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tipo:</td>
                <td>Trebbiano & Chardonnay </td>
              </tr>              
              <tr>
                <td class='tdHeader'>Altimetria</td>
                <td>200 metri</td>
              </tr>
              <tr>
                <td class='tdHeader'>Esposizione e tipologia suolo</td>
                <td>Est-ovest. Media strutturato, terra plioceniche</td>
              </tr>
              <tr>
                <td class='tdHeader'>Densit&agrave; impianti</td>
                <td>3500 ceppi/ha</td>
              </tr>
              <tr>
                <td class='tdHeader'>Sistema allevamento</td>
                <td>capovolto toscano e cordone speronato</td>
              </tr>
              <tr>
                <td class='tdHeader'>Et&agrave; media vigneto</td>
                <td>15 anni</td>
              </tr>
            </table>
            <b>Vinificazione e affinamento</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Resa ha </td>
                <td>110 quintali di uva</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vendemmia</td>
                <td>Settembre, meccanica</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pigiatura delle uve</td>
                <td>Pressa pneumatica</td>
              </tr>
              <tr>
                <td class='tdHeader'>Temperatura e durata della fermentazione</td>
                <td>18°C per 10 giorni in acciaio inox</td>
              </tr>
             <tr>
                <td class='tdHeader'>Fermentazione malolattica</td>
                <td>No</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento</td>
                <td>6 mesi in acciaio inox</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grado alcolico</td>
                <td>12.5% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento minimo in bottiglia</td>
                <td>2 mesi</td>
              </tr>
            </table>
            <b>Note di degustazione</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Colore</td>
                <td>Giallo paglierino</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profumi</td>
                <td>Frutti tropicali con note di canditi</td>
              </tr>
              <tr>
                <td class='tdHeader'>Gusto</td>
                <td>Fresco, di buona struttura acida e persistenza</td>
              </tr>
              <tr>
                <td class='tdHeader'>Abbinamenti</td>
                <td>Pesce, aperitivi e carni bianche</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>

