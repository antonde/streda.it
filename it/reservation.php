<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Request Availability</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
	    $(function() {
		    $( "#arriving" ).datepicker();
		    $( "#departing" ).datepicker();
	    });
	  </script>
	<script type="text/javascript">
    $(document).ready(function() {
      $("#contact_form").validate({
        rules: {
          Name: "required",//name field validate
          Email: {// compound rule
	          required: true,
	          email: true
	        },
	      Country: {
		          required: true,
		        },
		  Arriving: {
	          required: true,
	        },
	      Departing: {
	          required: true,
	        },
	      NumberofAdults: {
		          required: true,
		        },
		  Numberofkidsunder13: {
			          required: true,
			        }
        },
        messages: {
            message: "Please enter a comment.",
  		  name:"Please enter your name."
  		 }
      });
    });

  </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
        	<?php 
          	if($_REQUEST['Email']){
	          	$to      = 'info@streda.it';  
				$subject = "Richiesta disponibilita'";
				$message = "<html>
							<head>
							</head>
							<body>
							  <p><b>Richiesta disponibilit&agrave;</b></p> 
							  <p>E-mail: ".$_REQUEST['Email']."</p> 
							  <p>Nome: ".$_REQUEST['Name']."</p> 
							  <p>Cognome: ".$_REQUEST['Surname']."</p> 
							  <p>Citt&agrave;: ".$_REQUEST['City']."</p> 
							  <p>CAP: ".$_REQUEST['CAP']."</p> 
							  <p>Stato: ".$_REQUEST['Country']."</p> 
							  <p>Telefono: ".$_REQUEST['PhoneNumber']."</p> 
							  <p>Arrivo: ".$_REQUEST['Arriving']."</p>
							  <p>Partenza: ".$_REQUEST['Departing']."</p>
							  <p>Numero di adulti: ".$_REQUEST['NumberofAdults']."</p> 
							  <p>Numero di bambini sotto i 13 anni: ".$_REQUEST['Numberofkidsunder13']."</p> 
							  <p>Appartamento: ".$_REQUEST['Unit']."</p> 
							  <p>Richieste speciali: ".$_REQUEST['SpecialRequests']."</p> 
							</body>
							</html> ";
				
				$headers = 'From: streda@streda.it' . "\n"; 
				$headers .= 'MIME-Version: 1.0' . "\n"; 
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n"; 
				
				mail($to, $subject, $message, $headers);
				
				echo '<p>La Vostra richiesta &egrave; stata inviata. Vi contatteremo il prima possibile</p>';
          	}
          	?>
          <div class='post'>
            <h1>Request Availability</h1>
            <form name='form' id="contact_form" method="post">
              <table class='form'>
                <tr>
                  <td>Email *</td>
                  <td><input type='text' name='Email'/></td>
                </tr>
                <tr>
                  <td>Nome *</td>
                  <td><input type='text' name='Name'/></td>
                </tr>
                <tr>
                  <td>Cognome</td>
                  <td><input type='text' name='Surname'/></td>
                </tr>
                <tr>
                  <td>Citt&agrave;</td>
                  <td><input type='text' name='City'/> CAP: <input type='text' name='CAP'/></td>
                </tr>
                <tr>
                  <td>Paese*</td>
                  <td><input type='text' name='Country'/></td>
                </tr>
                <tr>
                  <td>Numero Telefono</td>
                  <td><input type='text' name='PhoneNumber'/></td>
                </tr>
                <tr>
                  <td>Periodo *</td>
                  <td>
                    da <input type="text" name="Arriving" id='arriving'/>
                    a <input type='text' name='Departing' id='departing'/>
                  </td>
                </tr>
                <tr>
                  <td>Numero di adulti*</td>
                  <td><input type='text' name='NumberofAdults'/></td>
                </tr>
                   <tr>
                  <td>Numero di bambini (0-13)*</td>
                  <td><input type='text' name='Numberofkidsunder13'/></td>
                </tr>
               
                <?php
                  if(isset($_GET["unit"])){
                    echo "<tr><td>Unit Requested</td><td>";
                    echo "<input type='text' name='Unit' value='".$_GET["unit"]."'/>";
                    echo "</td></tr>";
                  }
                ?>
                <tr>
                  <td>Richieste speciali</td>
                  <td><textarea name='SpecialRequests' style='width:90%;min-height:120px;'></textarea></td>
                </tr>
              </table>
              <div style='text-align:center;'>
                <input type='submit' value='Send' id='submit'/>
              </div>
              * CAMPI OBBLIGATORI
              <h2>Contatti</h2>
             <p>
            Streda Belvedere Azienda Agricola</br>
			50059 Streda-Vinci (FI)</br>
			Tel+39 0571 729195 Fax +39 0571 568563</br> 
			E-mail streda@streda.it</br>
            </p>
              </p>
            </form>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
