<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Home</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/image1.jpg',
          'images/image2.jpg',
          'images/image3.jpg',
          'images/image4.jpg',
          'images/image5.jpg',
          'images/image6.jpg',
        ]);
      });
      imgDir = "";
    </script>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='content'>
        <div class='imagePreview'>
          <img id='oldImage' src='images/image1.jpg'/>
          <img id='newImage' src='images/image1.jpg' class='previewLoader'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/image6.png' onclick='changePreview(6)' /></li>
              </ul>
            </div>
          </div>
        </div>
        <a href='accomodations.php'><img src='images/buttonFancy.png'/></a>
        <div class='post'>
          <h1>Un Borgo Medievale</h1>
          <p>
Si trova in una delle località più suggestive della Toscana, nel cuore del Chianti, sulle pendici delle colline del Montalbano, a pochi minuti dal centro di Vinci, città natale dell'illustre maestro Leonardo da Vinci.  Nascosto tra 80 ettari di vigneti e oliveti, STREDA BELVEDERE è un'oasi di pace e quiete.  Vini di qualità, olio extra vergine d'oliva e grappa sono prodotti dall'Azienda Agricola, viti e ulivi sono stati coltivati ​​a Streda Belvedere per molte generazioni.  Ser Piero, padre di Leonardo da Vinci, cresceva i vitigni che avvolgono la tenuta dal XVI secolo.</p> 
          <img src='images/article01.png'/>
          <p> Il romantico borgo si sviluppò in epoca romanica, intorno alla chiesa di San Bartolomeo a Streda, i cui resti più antichi risalgono a circa l'anno 1000 ed è stato restaurato negli ultimi anni e offre un'opportunità unica per trascorrere una vacanza immersi nella storia e nella natura.
          </p>
          <a href='history.php'>Per sapere di più su Streda Belvedere</a>, or <a href='accomodations.php'>Per vedere gli appartamenti</a>.
        </div>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
