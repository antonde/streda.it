<h1>I nostri prodotti</h1>
<img src='images/wineSmall.png' style='float:left;'/>
<p>
  Olio, vino e grappa prodotti direttamente con i frutti della nostra terra.<br/>
  <a href='products.php'><img src='images/buttonBrowse.png'/></a>
</p>
<hr/>
<h1>Accoglienza:</h1>
<p>
Più di 10 soluzioni abitative di prestigio immerse nell'incatevole atmosfera delle colline di Vinci.
</p>
<a href='accomodations.php'><img src='images/buttonComeStay.png'/></a>
<img src='images/sideimage1.jpg'/>
  <a href="http://pictures.streda.it" target="_new"><img src='images/gallery.png'/></a>
<hr/>
<h1>Dal Nostro Blog:</h1>
<?php
        require_once('../simplepie/simplepie.inc');
        $feed = new SimplePie();
$feed->set_feed_url(array('http://blog.streda.it/feed'));
        $feed->init();
        $feed->handle_content_type();
        $feed->set_item_limit(3);
?>
<?php
	foreach ($feed->get_items() as $item):
?>
	<div class="item">
        <h2><a href="<?php echo $item->get_permalink(); ?>"><?php echo $item->get_title(); ?></a></h2>
        <p><?php echo $item->get_description(); ?><a href="<?php echo $item->get_permalink(); ?>"> More</a></p>
        <!-- <p><small>Posted on <?php echo $item->get_date('j F Y | g:i a'); ?></small></p> -->
	</div>
<?php endforeach; ?>
