<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Grappa</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>Grappa del Chianti</h1>
            <h1>GOCCE DI LUNA</h1>
            <img src='images/grappa1.png'/>
            <p>
              Ottenuto dalla distillazione delle aromatiche vinacce del Chianti DOCG provenienti dalla tenuta di Streda ad una fermentazione guidata esprime intatte tutte la fragranza e le caratteristiche dell’uvaggio originario che sono ancora più esaltate dalla distillazione in ambienti sottovuoto.
              <br/><br/>
              <b>Caratteristiche organolettiche:</b><br/>
              Profumo fine fruttato e floreale misuralmente aromatico. 
              <br/><br/>
              <b>Sapore:</b><br/>
              Leggero e armonico
              <br/><br/>
              <b>Gradazione Alcolica</b><br/>
              40°
              <br/><br/>
              <b>Bottiglie</b><br/>
              Size/ml 700
            <p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
