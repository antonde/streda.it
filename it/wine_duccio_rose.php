<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Duccio Rosé</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Nostro Vino</a> » Duccio Rosé
          </div>
          <div class='post'>
            <h1>Duccio di streda belvedere rose</h1>
            <p>
              <h2>Toscana rosè igp(igt)</h2>
            <p>
            <img src='../images/wines/duccio_rose.png' style='float:left;'/>
            <b>Caratteristiche territorio di produzione</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Produttore</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tipo</td>
                <td>Prevalentemente Sangiovese</td>
              </tr>
              <tr>
                <td class='tdHeader'>Altimetria</td>
                <td>200 metri</td>
              </tr>
              <tr>
                <td class='tdHeader'>Esposizione e tipologia suolo</td>
                <td>Est-Ovest. Medio impasto</td>
              </tr>
              <tr>
                <td class='tdHeader'>Densit&agrave; impianti</td>
                <td>5000 ceppi/ettaro</td>
              </tr>
              <tr>
                <td class='tdHeader'>Sistema allevamento</td>
                <td>Cordone speronato</td>
              </tr>
              <tr>
                <td class='tdHeader'>Et&agrave; media vigneto</td>
                <td>15 anni</td>
              </tr>
            </table>
            <b>Vinificazione e affinamento</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Resa ha </td>
                <td>110 quintali di uva</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vendemmia</td>
                <td>Metà settembre, raccolto meccanicamente</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pigiatura delle uve</td>
                <td>Con diraspatrice e pressatura soffice</td>
              </tr>
              <tr>
                <td class='tdHeader'>Temperatura e durata della fermentazione</td>
                <td>Alla temperatura controllata di 17° C in botti d'acciaio per 20 giorni</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tempi macerazione sulle bucce</td>
                <td>No</td>
              </tr>
             <tr>
                <td class='tdHeader'>Fermentazione malolattica</td>
                <td>No</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento</td>
                <td>Almeno 6 mesi in botti d'acciaio.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grado alcolico</td>
                <td>12.5% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento minimo in bottiglia</td>
                <td>3 mesi</td>
              </tr>
            </table>
            <b>Note di degustazione</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Colore</td>
                <td>Rosa chiaro</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profumi</td>
                <td>Frutta fresca e floreale, ben saldi</td>
              </tr>
              <tr>
                <td class='tdHeader'>Gusto</td>
                <td>Morbido e rotondo al palato, con tannini morbidi e una finitura eccellente, un vino molto armonioso</td>
              </tr>
              <tr>
                <td class='tdHeader'>Abbinamenti</td>
                <td>Antipasti, frutti di mare antipasti, italiano di antipasti, primi piatti e ogni tipo di insalata, zuppa di pomodoro, pesce e carni bianche</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
