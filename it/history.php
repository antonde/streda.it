<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - History</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post' style='width:95%;'>
            <h1>Vinci</h1>
            <p>Streda Belvedere si trova a pochi minuti da VINCI, paese natale di Leonardo Da Vinci, uomo d'ingengno e di talento universale del Rinascimento. La città di Vinci è poco più di un borgo sviluppatosi attorno al cestello medievale dei conti Guidi e dominato dalla torre che costituisce un elemento caratterizzante nel paesaggio collinare. Il nucleo centrale della rocca, restaurato nel 1940, vede dal 1953  al suo interno le sale del Museo Leonardiano che, riallestito nel 1986 e nel 2004, espone un'ampia raccolta di modelli di macchine tratti da disegni e schizzi di Leonardo. Conserva anche una terracotta invetriata della Bottega di Giovanni della Robbia. E' strettamente connesso con la Palazzina Uzielli e con la Biblioteca Leonardiana, che costituisce un importante centro di documentazione per gli studi su Leonardo. Dal 1993 negli spazi evocativi della Galleria sotterranea e nelle antiche cantine del Castello di Vinci è aperto al pubblico il Museo Ideale Leonardo Da Vinci.</p>
            <img src='images/vinci1.jpg'/>
            <p> E' il primo museo al mondo che tratta la complessità di Leonardo artista, scienziato, inventore e designer in rapporto alla sua biografia, ai suoi territori e alla sua attualità, con originali antichi (dipinti della Bottega di Leonardo, incisioni e reperti). Oltre sessanta modelli recentemente ricostruiti ad arte dai progetti di Leonardo (alcuni funzionanti, altri praticabili) e meraviglie del Leonardismo (da cimeli d'epoca a opere di artisti moderni compreso Duchamp)…</p> <p> I visitatori del Museo Ideale potranno accedere (dalla via Collinare alle porte di Vinci) al nascente "Giardino di Leonardo" ed entrare nel "Labirinto dei Vinci", (1.500 alberi e un percorso di 740 metri) oltre che percorrere il "Nodo infinito" e il "Sentiero di alberi e fiori diversi". del giardino dei sensi, dei segreti, delle cosmologie e della scoperta, come sezione a cielo aperto del Museo Ideale e parco tematico.</p> <p> Entro le mura castellane si trova la chiesa di Santa Croce, d'origine medievale ma ristrutturata in stile neo-rinascimentale nella prima metà del Novecento, dove nell'aprile 1452 fu battezzato Leonardo. Vi si trovano il "Fonte Battesimale" ricomposto nel 1952, quinto centenario della nascita dell'artista-scienziato, e alcuni dipinti del XVIII e XVII secolo. In fondo all'attuale via Giovanni XXIII si erge il Santuario secentesco della SS. Annunziata, con una grande pala d'altare attribuita a Fra' Paolino da Pistoia: una "Annunciazione" del primo Cinquecento. Sui suggestivi colli di Anchiano, a due chilometri da Vinci, si trova una costruzione rurale (restaurata nel 1952 e nel 1986) che una tradizione identifica con la casa natale di Leonardo. A Pochi chilometri da Vinci, a Sant'Ansano, sorge la pieve romanica a tre navate di S. Giovanni Battista in Greti, che conserva ancora i capitelli medievali scolpiti e un capolavoro di Rutilio Manetti, uno dei maestri della pittura senese del Seicento. Vinci è il luogo ideale dove scoprire le opere, l'arte e lo spirito di Leonardo, ed è diventata un vero punto di riferimento per tutti gli appassionati del genio rinascimentale. </p> 
           <h1>Streda</h1><h2> La via tra Vinci e Cerreto Guidi</h2>
           <img src='images/history1.jpg'/>
            <p>Si deve a Leonardo Da Vinci, una delle più antiche mappe che raffigura il territorio compreso tra i torrenti Streda e Vincio . Qui abitarono antiche popolazioni preistoriche, ma fu l'importanza romana a caratterizzare la Val di Streda. San Bartolomeo di Stradona, poi di Streda, fu una delle chiese romaniche edificate dopo l'anno 1000.</p> 
                                                                                                                
            <p>I documenti storici fanno risalire le prime notizie della chiesa al XIII° secolo, coeva al Castello di Vinci, come del resto la vicina chiesa di San Pantaleo, il toponimo indica l'esistenza di una strada, probabilmente antichissima, forse addirittura romana che attraversava il piccolo borgo. Altri toponimi limitrofi confermano l'esistenza di questa importante arteria stradale, ben prima dell'anno Mille, della quale tuttavia sembrano essere perdute le tracce. Streda si trovava quindi in un luogo nevralgico per i commerci ed il passaggio delle persone (forse anche di pellegrini) che da Pistoia e dal Montalbano si dirigevano verso San Miniato e la Valdelsa.</p>
              
            <p>Campo Streda fu proprietà della famiglia Guidi fino al 1254,quando la proprietà fu trasferita al Comune di Firenze. Negli Statuti e riforme del Comune di Vinci è evidente l'importanza della Chiesa di San Bartolomeo , quest'ultima costudiva una delle quattro chiavi delle casse in cui erano racchiuse "le borse" con i nomi di quanti partecipavano all'elezione delle cariche pubbliche.</p>
            <p>Una carta dei capitani di parte Guelfa della seconda metà del Cinquecento raffigura e descrive le strade e gli edifici, poche unità ma di grande interesse storico, di quel "popolo" di Santa Maria a Streda in cui nel XV secolo,aveva possedimenti anche il notaio Piero Da Vinci, padre del grande Leonardo.  La storia locale individua in Bartolomeo un nome importante per le storie cittadine. Il ricordo torna quindi a quel Bartolomeo di Domenico Santini che con il suo lascito testamentario nel 1615 consentì di abbellire la costruzione del primo oratorio, detto dell'Assunta, a Vinci. Forse indusse anche l'arrivo della famosa effige dell'Annunziata che alla fine ne cambiò l'intitolazione. Con i suoi lasciti testamentari, in ogni caso, la chiesina - oggi della SS. Annunziata - ebbe finalmente il suo primo Cappellano, Leone Micheletti, ovvero il parroco di San Bartolomeo a Streda ( a cui il Santini aveva lasciato alcune sostanze) , che ogni mercoledì da Streda si recava a Vinci e chiamava al suono di una "campanina" la popolazione ai santi uffizi ( cfr. A. Mazzanti Il piccolo Santuario della Madonna in Vinci Fiorentino, 1924). L'usanza del parroco di San Bartolomeno a Streda di celebrare nel Santuario di Vinci è durata fino al XX° secolo, ovvero fino all'ultimo parroco titolare.</p>
            <img src='images/2streda.jpg'/>
            <p>Lo stato della chiesa di San Bartolomeo in Streda è oggi molto precario, pur essendo stata nel passato una chiesa ricca di patronati. Alcuni Da Vinci addirittura hanno vissuto in questo popolo. Nuove strade che collegano Vinci a Cerreto Guidi sono sorte più distanti, lasciando San Bartolomeo a Streda, in vetta alla sua collina in aristocratica, quanto decadente, solitudine. Dell'antica stradona non sembra esserci traccia.</p>
            <p>Il culto locale di San Bartolomeo, dall'antichità  nel comune di Vinci seppure in ambito ecclesiastico sotto la Diocesi di Lucca, prima, e di San Miniato, poi, consente di individuare per il territorio e popoli di riferimento origini antichissime rispetto all'intero territorio comunale, con dei punti di contatto ed analogia con l'altro versante pistoiese del Montalbano.</p>
            <p>Ogni 24 agosto nel contado vinciano ( o vinciarese) la gente del posto continua a ricordare. </p> 
            <br clear='both'/>
                 </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
