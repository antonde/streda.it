<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Appartamenti</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-16px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            
            <h1>La nostra casa vacanze</h1>
            <h2>"Se le arti e la cultura sono l'essenza dell'essere umano, il vino è l'essenza e la glorificazione della terra"</h2> <p>così esordisce Leonardo da Vinci in uno dei suoi scritti, ed è dal concetto di vino come cultura che nasce Streda Belvedere.</p>
<p>La Casa Vacanze è composta di 3 casali storici ristrutturati secondo le antiche tradizioni toscane.  Il vasto territorio comprende 80 ettari di vigneti e un piccolo lago privato, a disposizione di tutti gli ospiti una bellissima piscina panoramica, una vasca idromassaggio esterna e barbecue. E 'ideale per coppie, famiglie con bambini e piccoli gruppi alla ricerca di una vacanza culturale e rilassante. Ci sono 4 camere e 13 appartamenti.</p>
<p>Incluso: Aria condizionatae riscaldamento con controlo climatico individuale, letto king size o queen size con un lussuoso materasso ricoperto di gomma  memorex, fresca biancheria di cotone, cuscini in gomma, cuscini in piuma, digital tv color piatta, frigorifero con congelatore, lavastoviglie e rete wireless (gratuitamente) disponibile.</p>
            <div style='margin:auto auto;'>
              <h2>Casa Belvedere</h2>
			<p>	
In un atmosfera accogliente vi aspettano 5 appartamenti spaziosi e arredati in stile classico toscano con lavabi e tavoli in marmo, complementi dalle tinte primaverili per un soggiorno più piacevole e rilassante. Avrete la garanzia di passare ore stupende immersi in un paesaggio da sogno.  La maggior parte degli appartamenti godono di una magnifica vista sulla terrazza e sulla piscina. </p>
             <div style="width:50%; float: left;">
             
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_merlot.php'><img class="apartment" src='images/merlot/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_merlot.php'>Appartamento 1: <span>Merlot</span></a></h3>
                    <a href='house_merlot.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_cabernet.php'><img class="apartment" src='images/cabernet/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_cabernet.php'>Appartamento 2: <span>Cabernet</span></a></h3>
                    <a href='house_cabernet.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_syrah.php'><img class="apartment" src='images/syrah/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_syrah.php'>Appartamento 3: <span>Syrah</span></a></h3>
                    <a href='house_syrah.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>              

			</div>
            <div style="width:50%; float: left;">
            


              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_sangiovese.php'><img class="apartment" src='images/sangiovese/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_sangiovese.php'>Appartamento 4: <span>Sangiovese</span></a></h3>
                    <a href='house_sangiovese.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_chardonnay.php'><img class="apartment" src='images/chardonnay/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_chardonnay.php'>Appartamento 5: <span>Chardonnay</span></a></h3>
                    <a href='house_chardonnay.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>                
            
            
            </div>
            <div class="clear"></div>
            
            
            
            
            </div>
            
            
            
            
            
            
            
            <div style='margin:auto auto;'>
              <h2>Casa Clarinetto</h2>
<p>Include 4 appartamenti e 4 camere, tutte arredate  in stile Swing con la collezione Miami swing, un'idea del noto artista e cantante Renzo Arbore e progettato dai designer Licheri e Cappellini, scenografi con esperienze a tutto campo dalla pubblicità al teatro, dal cinema alla televisione, noti alcuni lavori prodotti per la Rai.  Con i suoi insoliti e unici interni per una casa in campagna, Casa Clarinetto  è ispirata al jazz e alla pop art, molto giovane, fresca e colorata, veramente qualcosa di diverso, destinata a ospitare chi è alla ricerca del meglio.	</p>
             
              
              <div style="width:50%; float: left;">
              
              
              
              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_clarinetto.php'><img class="apartment" src='images/clarinetto/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_clarinetto.php'>Appartamento 10: <span>Clarinetto</span></a></h3>
                    <a href='house_clarinetto.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_sassophone.php'><img class="apartment" src='images/sassophone/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_sassophone.php'>Appartamento 11: <span>Sassofono</span></a></h3>
                    <a href='house_sassophone.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_orchestra.php'><img class="apartment" src='images/orchestra/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_orchestra.php'>Appartamento 12: <span>Orchestra</span></a></h3>
                    <a href='house_orchestra.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>              

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_tromba.php'><img class="apartment" src='images/tromba/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_tromba.php'>Appartamento 13: <span>Tromba</span></a></h3>
                    <a href='house_tromba.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>   

				</div>
                <div style="width:50%; float: left;">


              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera1.php'><img class="apartment" src='images/camera1/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera1.php'>Swing 1:<br><span>Blue Room</span></a></h3>
                    <a href='house_camera1.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera2.php'><img class="apartment" src='images/camera2/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera2.php'>Swing 2:<br><span>Orange Room</span></a></h3>
                    <a href='house_camera2.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera3.php'><img class="apartment" src='images/camera3/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera3.php'>Swing 3:<br><span>Violet Room</span></a></h3>
                    <a href='house_camera3.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>

              <div class="apartmentWrap">
              	<div class="left">
                	<a href='house_camera4.php'><img class="apartment" src='images/camera4/image1.png'/></a>
              	</div>
                <div class="right">
                  <h3><a href='house_camera4.php'>Swing 4:<br><span>Green Room</span></a></h3>
                    <a href='house_camera4.php'><img src="images/button-view.png" width="80" height="27" alt="View"></a>
                </div>
                <div class="clear"></div>
		      </div>
              </div>
             
             <div class="clear"></div>
             <h1>La nostra casa vacanze</h1>
             <p>Ogni appartamento è dotato dei più moderni confort, ed è immerso altresì in una piacevolissima atmosfera, fra cultura contadina, spontanea accoglienza e ospitalità, unito a delizie per il palato e all'arte insieme a buon vino, olio e grappa sapientemente prodotti dall'azienda agricola Streda Belvedere.</p>
<p>Gli spazi della casa vacanze Streda Belvedere sono stati arricchiti da opere di artisti e di famosi designer per garantire agli ospiti una vacanza veramente esclusiva, profumi e sapori che ben si accordano con le allegre note jazz degli appartamenti Miami Swing by Renzo Arbore. </p>
<p>Questo progetto riunisce eccellenza e valorizza una vocazione innata: quella dell'attenzione al design e al made in italy. La qualità dei servizi e della struttura contribuiscono a rendere confortevole la permanenza ad ogni tipo di viaggiatore. Tutti gli appartamenti sono dotati di climatizzazione caldo/freddo, televisore split, lavastoviglie, connessione wifi e la maggior parte di loro hanno letti king size. Gli ospiti possono usufruire di una stanza lavanderia,con lavatrice e asciugatrice.
Non disponiamo di ristorante, ma nelle vicinanze ce ne sono di ottimi, e saremo lieti di consigliarvi ogni giorno con proposte nuove per farvi gustare le migliori ricette toscane.</p>
<p>Anche se immersi nella campagna toscana dove regna la privacy e la tranquillità, l'ideale per una vacanza rilassante , siamo comunque a pochi minuti dal centro di Vinci e a circa un chilometro dal nuovo golf club inaugurato nell'Aprile 2009.</p> 
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
