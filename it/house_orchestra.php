<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Orchestra</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/orchestra/image1.jpg',
          'images/orchestra/image2.jpg',
          'images/orchestra/image3.jpg',
          'images/orchestra/image4.jpg',
          'images/orchestra/image5.jpg',
          'images/orchestra/image6.jpg',
          'images/orchestra/image7.jpg',
          'images/orchestra/image8.jpg',
          'images/orchestra/image9.jpg',
          'images/orchestra/image10.jpg',
        ]);
      });
      imgDir = "orchestra";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <?php
            include("sidebarHousing.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php'>Alloggi</a> » Orchestra
          </div>
          <h1>Orchestra</h1>
        <div class='imagePreview'>
          <img id='newImage' src='images/orchestra/image1.jpg' class='previewLoader'/>
          <img id='oldImage' src='images/orchestra/image1.jpg'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/orchestra/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/orchestra/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/orchestra/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/orchestra/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/orchestra/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/orchestra/image6.png' onclick='changePreview(6)' /></li>
                <li><img src='images/orchestra/image7.png' onclick='changePreview(7)' /></li>
                <li><img src='images/orchestra/image8.png' onclick='changePreview(8)' /></li>
                <li><img src='images/orchestra/image9.png' onclick='changePreview(9)' /></li>
                <li><img src='images/orchestra/image10.png' onclick='changePreview(10)' /></li>
              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>Dettagli:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Tipo </td>
                <td>appartamento di 2 stanze, 56m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di posti letto :</td>
                <td>4 (2+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di camere da letto :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di bagni:</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>Piano / terrazzo:</td>
                <td>Piano terra, patio privato (40 mq) con tavolo, sedie e sdraio.  </td>
              </tr>
              <tr>
                <td class='tdHeader'>Locale :</td>
                <td>In campagna con un giardino comune di 5000 m² in una proprietà di 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composto da:</td>
                <td>Soggiorno - sala da pranzo con divano letto matrimoniale e angolo cottura, 1 camera doppia (letto matrimoniale con un lussuoso materasso ricoperto di gomma MEMOREX), fresca biancheria di cotone, 2 bagni con doccia </td>
              </tr>
              <tr>
                <td class='tdHeader'>Angolo cottura dotato di:</td>
                <td>Lavastoviglie, frigorifero, congelatore, 4 fornelli cucina (gas) e un tostapane.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Appartamento dotato di :</td>
                <td>Aria condizionata e riscaldamento individuale in ogni stanza,  TV LCD digitale piatta, asciugacapelli, e rete Wireless disponibile gratuitamente..</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vista :</td>
                <td>Campagna</td>
              </tr>
              <tr>
                <td class='tdHeader'>L'accesso dei disabili / bagno per disabili:</td>
                <td>No</td>
              </tr>
            </table>
			<h2>Orchestra Plus - Questa camera può essere combinato con the <a href="house_camera3.php" target="_blank">Violet Room</a>:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Tipo </td>
                <td>Apartamenti 3 stanze, 75m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di posti letto :</td>
                <td>6 (4+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di camere da letto :</td>
                <td>2 (Uno è al primo piano con ingresso indipendente e senza porta di collegamento).</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di bagni:</td>
                <td>3</td>
              </tr>
              <tr>
                <td class='tdHeader'>Piano / terrazzo:</td>
                <td>Piano terra e al 1 ° piano, patio privato (40 mq) con tavolo, sedie e sdraio.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Locale :</td>
                <td>In campagna con un giardino comune di 5000 m² in una proprietà di 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composto da:</td>
                <td>Al piano terra: soggiorno / sala da pranzo con divano letto matrimoniale e angolo cottura, 1 camera matrimoniale (letto matrimoniale con un lussuoso materasso ricoperto di gomma MEMOREX), fresca biancheria di cotone, 2 bagni con doccia; Al Primo Piano (con ingresso Indipendente e Senza porta di Collegamento): 1 camera matrimoniale (Letto matrimoniale), 1 Bagno con doccia.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Angolo cottura dotato di:</td>
                <td>Lavastoviglie, frigorifero, congelatore, 4 fornelli cucina (gas) e un tostapane.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Appartamento dotato di :</td>
                <td>Aria condizionata e riscaldamento individuale in ogni stanza,  TV LCD digitale piatta, asciugacapelli, e rete Wireless disponibile gratuitamente..</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vista :</td>
                <td>Campagna</td>
              </tr>
              <tr>
                <td class='tdHeader'>L'accesso dei disabili / bagno per disabili:</td>
                <td>Si (Solo piano terra) </td>
              </tr>
            </table>
            <p>*Si prega di notare che appartamento l'Orchestra si trova al piano terra, mentre la stanza Viola  è al primo piano e ha un altro ingresso. Non c'è nessuna porta di connessione. Per questo motivo l'appartamento è appropriato solo per adulti o famiglie con figli indipendenti.</p>            

            <h2>Piscina:</h2>
            <img src='images/pool1.jpg'/>
            <table>
              <tr>
                <td class='tdHeader'>Dimensioni :</td>
                <td>14m x 7m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profondità :</td>
                <td>Minimo 1m; massima 1,40 m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Periodo di apertura :</td>
                <td>Dal 31 marzo fino al 13 ottobre</td>
              </tr>
              <tr>
                <td class='tdHeader'>Orari di apertura :</td>
                <td>Da 8.30h fino 18.30h</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pulizia delle piscine :</td>
                <td>Quotidiano</td>
              </tr>
              <tr>
                <td class='tdHeader'>Jacuzzi :</td>
                <td>Condiviso, dimensioni 2,20 m x 2,20 m, profondità 0,50 m</td>
              </tr>
            </table>

            <h2>Servizi:</h2>
            Lavanderia con lavatrice, asciugatrice e ferro da stiro, connessione wi-fi, parcheggio, area giochi per bambini (in costruzione, pronto per la stagione 2012), 2 barbecue in comune e doccia esterna in piscina.
            <br/><br/>
            <a href='reservation.php?unit=Orchestra'><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
