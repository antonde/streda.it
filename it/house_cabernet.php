<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Cabernet</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/cabernet/image1.jpg',
          'images/cabernet/image2.jpg',
          'images/cabernet/image3.jpg',
          'images/cabernet/image4.jpg',
          'images/cabernet/image5.jpg',
          'images/cabernet/image6.jpg',
          'images/cabernet/image7.jpg',
          'images/cabernet/image8.jpg',
          'images/cabernet/image9.jpg',
          'images/cabernet/image10.jpg',
          'images/cabernet/image11.jpg',
          'images/cabernet/image12.jpg',
          'images/cabernet/image13.jpg',
          'images/cabernet/image14.jpg',
          'images/cabernet/image15.jpg'
        ]);
      });
      imgDir = "cabernet";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <?php
            include("sidebarHousing.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php'>Alloggi</a> » Cabernet
          </div>
          <h1>Cabernet</h1>
        <div class='imagePreview'>
          <img id='newImage' src='images/cabernet/image1.jpg' class='previewLoader'/>
          <img id='oldImage' src='images/cabernet/image1.jpg'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/cabernet/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/cabernet/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/cabernet/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/cabernet/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/cabernet/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/cabernet/image6.png' onclick='changePreview(6)' /></li>
                <li><img src='images/cabernet/image7.png' onclick='changePreview(7)' /></li>
                <li><img src='images/cabernet/image8.png' onclick='changePreview(8)' /></li>
                <li><img src='images/cabernet/image9.png' onclick='changePreview(9)' /></li>
                <li><img src='images/cabernet/image10.png' onclick='changePreview(10)' /></li>
                <li><img src='images/cabernet/image11.png' onclick='changePreview(11)' /></li>
                <li><img src='images/cabernet/image12.png' onclick='changePreview(12)' /></li>
                <li><img src='images/cabernet/image13.png' onclick='changePreview(13)' /></li>
                <li><img src='images/cabernet/image14.png' onclick='changePreview(14)' /></li>
                <li><img src='images/cabernet/image15.png' onclick='changePreview(15)' /></li>
              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>Dettagli:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Tipo </td>
                <td>Apartamenti 3 stanze, 72m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di posti letto :</td>
                <td>6 (4+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di camere da letto :</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>Numero di bagni:</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Piano / terrazzo:</td>
                <td>1<sup>st</sup> piano, terrazzo in comune (100 mq) al piano terra con tavolo, sedie e sdraio.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Locale :</td>
                <td>In campagna con un giardino comune di 5000 m² in una proprietà di 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composto da:</td>
                <td>Soggiorno con angolo cottura e divano letto 1 camera matrimoniale (letto matrimoniale con un lussuoso materasso ricoperto di gomma MEMOREX), fresca biancheria di cotone, 1 camera doppia (letto matrimoniale con un lussuoso materasso ricoperto di gomma MEMOREX),  1 bagno con doccia.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Angolo cottura dotato di:</td>
                <td>Lavastoviglie, frigorifero, congelatore, microonde, 4 fornelli cucina (gas) e un tostapane.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Appartamento dotato di :</td>
                <td>Aria condizionata e riscaldamento individuale in ogni stanza,  TV LCD digitale piatta, asciugacapelli, e rete Wireless disponibile gratuitamente.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vista :</td>
                <td>Campagna</td>
              </tr>
              <tr>
                <td class='tdHeader'>L'accesso dei disabili / bagno per disabili:</td>
                <td>No</td>
              </tr>
            </table>

            <h2>Piscina:</h2>
            <img src='images/pool1.jpg'/>
            <table>
              <tr>
                <td class='tdHeader'>Dimensioni :</td>
                <td>14m x 7m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profondità :</td>
                <td>Minimo 1m; massima 1,40 m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Periodo di apertura :</td>
                <td>Dal 31 marzo fino al 13 ottobre</td>
              </tr>
              <tr>
                <td class='tdHeader'>Orari di apertura :</td>
                <td>Da 8.30h fino 18.30h</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pulizia delle piscine :</td>
                <td>Quotidiano</td>
              </tr>
              <tr>
                <td class='tdHeader'>Jacuzzi :</td>
                <td>Condiviso, dimensioni 2,20 m x 2,20 m, profondità 0,50 m</td>
              </tr>
            </table>

            <h2>Servizi:</h2>
            Lavanderia con lavatrice, asciugatrice e ferro da stiro, connessione wi-fi, parcheggio, area giochi per bambini (in costruzione, pronto per la stagione 2012), 2 barbecue in comune e doccia esterna in piscina.
            <br/><br/>
            <a href='reservation.php?unit=Cabernet'><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
