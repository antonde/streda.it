<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Syrah</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Nostro Vino</a> » Syrah
          </div>        
          <div class='post'>
            <h1>Drufo Syrah di Streda Belvedere</h1>
            <p>
              <h2>Toscana rosso igp(igt)</h2>
            <p>
            <img src='images/wines/syrah.png' style='float:left;'/>
            <b>Caratteristiche territorio di produzione</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Produttore</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tipo</td>
                <td>Syrah</td>
              </tr>
              <tr>
                <td class='tdHeader'>Altimetria</td>
                <td>200 metri</td>
              </tr>
              <tr>
                <td class='tdHeader'>Esposizione e tipologia suolo</td>
                <td>Sud, Sud-Ovest. Medio impasto con fossili calcarei</td>
              </tr>
              <tr>
                <td class='tdHeader'>Densit&agrave; impianti</td>
                <td>4500 ceppi/ha</td>
              </tr>
              <tr>
                <td class='tdHeader'>Sistema allevamento</td>
                <td>Cordone speronato</td>
              </tr>
              <tr>
                <td class='tdHeader'>Et&agrave; media vigneto</td>
                <td>15 anni</td>
              </tr>
            </table>
            <b>Vinificazione e affinamento</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Resa ha </td>
                <td>70 quintali di uva</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vendemmia</td>
                <td>Met&agrave; settembre, meccanica</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pigiatura delle uve</td>
                <td>Con diraspatrice </td>
              </tr>
              <tr>
                <td class='tdHeader'>Temperatura e durata della fermentazione</td>
                <td>32°C per 7 giorni in acciaio inox</td>
              </tr>
              <tr>
                <td class='tdHeader'>Tempi macerazione sulle bucce</td>
                <td>20 giorni</td>
              </tr>
             <tr>
                <td class='tdHeader'>Fermentazione malolattica</td>
                <td>Si</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento</td>
                <td>12 mesi in piccole botti di rovere francese</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grado alcolico</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Affinamento minimo in bottiglia</td>
                <td>6 mesi</td>
              </tr>
            </table>
            <b>Note di degustazione</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Colore</td>
                <td>Rosso rubino intenso</td>
              </tr>
              <tr>
                <td class='tdHeader'>Profumi</td>
                <td>Note floreali e fruttate in armonia con spezie e sentori di vaniglia</td>
              </tr>
              <tr>
                <td class='tdHeader'>Gusto</td>
                <td>Elegante e scorrevole in bocca, con una ricca trama tannica impreziosita 
	        dall&rsquo;apporto del legno, con tannini dolci e lungo retrogusto</td>
              </tr>
              <tr>
                <td class='tdHeader'>Abbinamenti</td>
                <td>Cacciagione, arrosto, formaggi stagionati</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
