<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Location</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
          <h1>Things to Do</h1>
            <h2>Vinci</h2>
            <img src='images/vinci.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Potrebbe iniziare la vostra visita nei dintorni di Streda Belvedere, lo splendido Borgo Medievale di Vinci. Continuando il percorso dalla Piazza Dei Conti Guidi ammirerete le opere del noto scultore e architetto Mimmo Paladino, la Chiesa di Santa Croce, di origine duecentesca, dove si dice che fu battezzato Leonardo da Vinci, il Museo leonardiano ospitato all'interno del Castello dei Conti Guidi e la torre dove si gode di un pittoresco paesaggio toscano. Attraverso un percorso verde tra gli ulivi si può raggiungere con una buona camminata di circa 30 minuti la Casa di Leonardo.</p>
			<div class="clear"></div>
            
            <h2>Golf</h2>
            <img src='images/golf.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Di fronte a Streda Belvedere da lato opposto della Via Provinciale, per gli amanti del golf c'è il Golf Club Bellosguardo. </p>
			<div class="clear"></div>
            
            <h2>Trekking</h2>
            <img src='images/hiking.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Inoltre, per gli amanti del trekking, nei nostri dintorni si trovano numerosi percorsi che si spingono fino a punti più alti del Montalbano (le carte sono disponibili alla reception). </p>
			<div class="clear"></div>
            
            <h2>Cerreto</h2>
            <img src='images/cerreto.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Nei dintorni di Cerreto Guidi, potete visitare la residenza di caccia della famiglia Medici costruita durante il Rinascimento e la famosa chiesa di S. Leonardo eretta sulle rovine romane. </p>
			<div class="clear"></div>          
          
            <h2>Firenze</h2>
            <img src='images/firenze.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Poi non mancare una visita alla città di Firenze e vi ricordo che il miglior modo è partire dalla stazione di Empoli dove potete trovare un treno ogni 15/30 minuti ed essere in centro di Firenze in una mezz'ora. Nei pressi della stazione di Empoli si trova un parcheggio e costo giornaliero è 3€ c.a. Anche il biglietto one way del treno per Firenze costa c.a. 3€. 
Il treno è un locale e non ha molti comforts, ma preso nelle ore non di punta è il modo più facile e agevole per arrivare a Firenze.
 </p>
			<div class="clear"></div>            

            <h2>Montecatini Terme</h2>
            <img src='images/montecatini.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Per visitare le famose terme recentemente ristrutturate circondate da un grande parco e un bellissimo giardino, l'entrata è a pagamento per poche ore o per tutto il giorno; salire con la funicolare fino alla suggestiva Piazzetta di Montecatini dove si trovano numerosi ristoranti e bar; oppure potete passare la giornata facendo shopping nei vari eleganti negozi aperti anche la domenica. </p>
			<div class="clear"></div>

            <h2>Viareggio e Forte dei Marmi</h2>
            <img src='images/viareggio.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Dista circa 70 km da Streda Belvedere e potete passare una giornata al mare nella bellissima Versilia.
È consigliabile in estate non andare di sabato e domenica, perché troppo affollata.</p>
			<div class="clear"></div>

            <h2>Pistoia</h2>
            <img src='images/pistoia.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>A 40 minuti da Streda Belvedere oltrepassate le colline del Montalbano c'è Pistoia, una città affollata e moderna dove si trovano meravigliosi palazzi antichi e circondata da mura romane.
Nel centro si trova una delle più importanti Cattedrali di esempio gotico in Italia con il Battistero, il Palazzo dei Vescovi e nelle vicinanze La Sala, una piccola piazza, antico ritrovo pistoiese dove ogni giorno c'è un interessante mercato ortofrutticolo. Un grande mercato di ogni genere di mercanzia, ha luogo ogni mercoledì mattina e sabato mattina in luglio ed agosto si svolge per le vie di centro e per il resto dell'anno nell'antica Piazza del Duomo. 
A Pistoia è piacevole attardarsi anche verso fine serata perché a quell'ora molti locali del centro si animano di avventori per la degustazione del classico aperitivo e che quando è bel tempo si attardano a cena nei tavolini che pullulano fuori dai ristoranti del centro di Pistoia.</p>
			<div class="clear"></div>            

            <h2>Pisa</h2>
            <img src='images/pisa.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Non ha bisogno di presentazione. Piazza dei Miracoli e la sua Torre sono facilmente raggiungibili con il treno dalla stazione di Empoli con un diretto o con la macchina percorrendo la superstrada FI PI LI. </p>
			<div class="clear"></div>   

            <h2>Siena</h2>
            <img src='images/Siena.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Da Streda Belvedere ci vogliono c.a. 2 ore in macchina, ma con il treno da Empoli (1 ora c.a.) è il modo più rilassante per raggiungere la città. </p>
			<div class="clear"></div>

            <h2>Altri luoghi da visitare in meno di 1 ora da Streda Belvedere sono:</h2>
            <ul>
              <li>
              Certaldo, la città di Bocaccio, San Miniato alto, San Gimignano con le sue famose torri.</li>
              <li>Lucca la città di 100 chiese e le mura più grandi e integri di epoca rinascimentale in Europa</li>
              <li>Montelupo Fiorentino con la sua ceramica maiolica rinascimentale, il centro di produzione di Firenze Medicea e il Museo della Ceramica, uno dei più importanti in Italia.</li> 
              <li>Con poco più di 1 ora da Streda si raggiunge Volterra, la famosa città etrusca.</li>
            </ul>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
