<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Duccio Bianco</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Our Wine</a> » >Duccio Bianco
          </div>
          <div class='post'>
            <h1>Duccio di Streda Belvedere Bianco</h1>
            <p>
              <h2>Toscana Bianco igp(igt)</h2>
            <p>
            <img src='images/wines/bianco.png' style='float:left;'/>
            <b>Charateristics of production area</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Producer</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape Type:</td>
                <td>Trebbiano & Chardonnay</td>
              </tr>              
              <tr>
                <td class='tdHeader'>Altitude</td>
                <td>200 metres</td>
              </tr>
              <tr>
                <td class='tdHeader'>Aspect and soil profile</td>
                <td>East-west. Medium textured, with Pliocene clay</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vineyard density</td>
                <td>3500 vines/hectare</td>
              </tr>
              <tr>
                <td class='tdHeader'>Training method</td>
                <td>Tuscan arched cane and spurred cordon</td>
              </tr>
              <tr>
                <td class='tdHeader'>Average vine age</td>
                <td>15 years</td>
              </tr>
            </table>
            <b>Fermentation and Maturation</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Yield per hectare </td>
                <td>110 quintals of grape</td>
              </tr>
              <tr>
                <td class='tdHeader'>Harvest period</td>
                <td>September, mechanically harvested</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape processing</td>
                <td>Pneumatic press</td>
              </tr>
              <tr>
                <td class='tdHeader'>Temp and length of fermentation</td>
                <td>18°C per 10 days in stainless steel</td>
              </tr>
             <tr>
                <td class='tdHeader'>Malolactic fermentation</td>
                <td>No</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maturation</td>
                <td>6 months in steel</td>
              </tr>
              <tr>
                <td class='tdHeader'>Alcohol</td>
                <td>12.5% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Minimun bottle ageing</td>
                <td>2 months</td>
              </tr>
            </table>
            <b>Tasting notes</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Appearance</td>
                <td>Straw yellow</td>
              </tr>
              <tr>
                <td class='tdHeader'>Nose</td>
                <td>Tropical aromas with muances of candied fruit</td>
              </tr>
              <tr>
                <td class='tdHeader'>Palate</td>
                <td>Crisp, refreshing, with lovely vein of acidity and a lingering finish</td>
              </tr>
              <tr>
                <td class='tdHeader'>Serving suggestions</td>
                <td>Fish, aperitifs, poultry and veal</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>

