<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - History</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post' style='width:95%;'>
            <h1>VINCI</h1> <h2>For centuries the area of Vinci has been famous for its oil and wine production.</h2>
            <p>
              Vinci, the fabled birthplace of Leonardo da Vinci on April 15, 1452, grew around the Guidi castle. The tower of the Guidi castle is typical for the hilly landscapes of Vinci, allowing the castle's inhabitants a clear view over the uneven landscape.</p>

<p>The Leonardo Museum has been running in the Guidi castle since 1952. This imposing castle underwent restoration in 1940, and renovations in 1986 and 2004. The Leonardo museum is filled with models of machines inspired by the drawings and sketches of Leonardo. Also on display is a magnificent ceramic piece from the Giovanni della Robbia workshop. The castle is connected to the Palazzina Uzielli and the Leonardo Library, which houses important documentation on da Vinci. </p>
<img src='images/vinci1.jpg'/>
<p>The Leonardo Da Vinci Ideal Museum has been open to the public since 1993, and is the first museum to explore the many facets of Leonardo da Vinci as an artist, scientist, inventor, and designer. On display are original paintings, engravings, and archeological finds from his workshop. The museum highlights his biography, and the relevance of his birthplace on his works and life. </p>
<p>Recently, over sixty models have been skillfully recreated from Leonardo's projects, some of them fully operational. There is also memorabilia of Leonardo's influence on artists such as Marcel Duchamp, who was particularly fascinated with Leonardo's works and life. Leonardo da Vinci's effects and influence on mass media and modern art  throughout history are also documented in this museum. The Ideal Museum includes an outdoor exhibit called the "Garden of Leonardo and Utopia". This garden includes the "Vinci Labrynth", the "Path of Different Trees and Flowers", and the "Endless Knot of Roses".</p>
<p>Within the walls of the castle one is able to visit the medieval Church of the Holy Cross, which was restored in the neo-Renaissance style during the first half of the 20th century. This church was the location of Leonardo da Vinci's baptism in April, 1452. You will be able to view the Baptismal Font, which was expertly restored in 1952 on the fifth centennial of Leonardo's birth, as well as a collection of paintings from the 17th and 18th centuries. At the end of Saint John the XXIII Street you will admire the Shrine of Annunziata, built in the 17th century. The shrine includes an altar piece created by artist Fra' Paolina da Pistoia in the 16th century representing "The Annunciation".</p>
<p>On the hills of the hamlet of Anchiano, which lies two kilometers from Vinci, there is a small stone farmhouse. This farmhouse, which was restored in 1952 and again in 1986, is believed to be the birthplace of Leonardo. At Sant'Ansano, near Vinci, you will find the Romanesque church of Saint John the Baptist in Greti, which houses medieval sculptures and a masterpiece by the famed master painter Rutilio Manetti, who was born in Siena during the 17th century.</p>

            </p>
            <h1>Streda</h1> <h2>The road between Cerreto Guidi and Vinci</h2>
            
              <img src='images/history1.jpg'/>
              <p>We owe the existence of one of the oldest maps of the areas between the Streda and Vincio rivers to none other than the great Leonardo da Vinci. Although prehistoric populations inhabited the area, it was the Romans who truly left their mark in the Streda Valley.</p>

<p>The church of San Bartolomeo is a Romanesque church which was built after the year 1000. It belonged to the dioceses of Lucca, and now rest under the control of San Miniato, bordering the dioceses of Pistoia and Florence.</p> </br></br></br>

<p></p><img src='images/2streda.jpg'/> <p>The community of Streda and its two mills were owned by the Guidi family until 1254, when the ownership was transferred to the municipality of Florence. One can understand the importance of the San Bartolomeo church by reading the 1382 statues of the municipality of Vinci. These statutes designated the church as the custodian of one of the four keys that opened the locked boxes containing the list of candidates for public office. This 16th century map of the Guelph Captains illustrates the few but interesting streets and building of Santa Maria in Strea, where Leonardo's father owned property in the 15th century.
            </p>
            <br clear='both'/>
            
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
