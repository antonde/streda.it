<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Location</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='sidebar' style='left:-30px;top:-20px;'>
        <?php
          include("sidebar.php");
        ?>
      </div>
      <div class='contentBG'>
        <div class='content'>
          <div class='post'>
          <h1>Things to Do</h1>
            <h2>Vinci</h2>
            <img src='images/vinci.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Start your tour with the beautiful medieval village of Vinci. As you continue down the path from the Piazza dei Cont Guidi, pause to admire the works of the famous architect and sculpture Mimmo Paladino. Visit the famous church of Santa Croce, built in the thirteenth-century, where it is said the famed Leonardo da Vinci himself was baptised. Explore the Leonardo Museum, housed in the looming Castle of the Counts, which features a soaring tower. From this tower one can experience an unparalled view of the picturesque Tuscan landscape. Meander for 30 minutes through a green, verdant path among the olive trees in order to reach the House of Leonardo. This walk will allow you to experience the same sights and sounds as the maestro himself during his youth.</p>
			<div class="clear"></div>
            
            <h2>Golf</h2>
            <img src='images/golf.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Facing the Streda Belvedere from the opposite side of Provincial Road is the Bellosgaurdo golf club, which features a 9-hole course. Gold lovers will feel at home on the manicured greens of the course. </p>
			<div class="clear"></div>
            
            <h2>Hiking</h2>
            <img src='images/hiking.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>For active travelers who love to feel the earth under their feet, there are numerous scenic hiking trails and paths which climb the highest point of Montalbano. Maps are available at the reception, allowing you to plan a route to your ability level and time constraints. </p>
			<div class="clear"></div>
            
            <h2>Cerreto</h2>
            <img src='images/cerreto.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Near the Cerreto Guidi is the hunting lodge of the Medici family, built during the Renaissance. As well, nearby is the famed church of St. Leonardo, which was built on Roman ruins. </p>
			<div class="clear"></div>          
          
            <h2>Florence by Train </h2>
            <img src='images/firenze.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>No visit to Tuscany is complete without a trip to the capital city of Florence. This can be easily and affordably done by train, as trains leave to Florence ever 15-30 minutes from the Empoli station. You will find yourself in the heart of the capital in 30 minutes, for only  € 3. For those who prefer to tour the countryside by car, there is a car park with a daily cost of approximately € 3 as well. The train is able to have such low fares by its lack of comforts and decadence, but it is by far the easiest and most convenient way to visit the capital. </p>
			<div class="clear"></div>            

            <h2>Montecatini Terme</h2>
            <img src='images/montecatini.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>The famous thermal baths have recently been renovated, and are surrounded by a large, luscious garden and a relaxing, scenic garden. Let your worries and stress melt away in the gentle, warm waters, or visit the gardens free of charge. When you become hungered by your travels, take the funicular up to the charming Piazzeta of Montecatini, where you will find numerous restaurants and bars to satisfy your appetites. There are also various elegant shops, which are even open on Sundays. </p>
			<div class="clear"></div>

            <h2>Viareggio e Forte dei Marmi</h2>
            <img src='images/viareggio.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>70 km from the Streda Belvedare is the beach in Versilia, where you can spend your day lounging on the warm sand in the sun. If you are travelling in the summer, plan a trip on a Saturday or Sunday, when the beach will not be so crowded. This will result in a much more relaxing trip to the warm waters.</p>
			<div class="clear"></div>

            <h2>Pistoia</h2>
            <img src='images/pistoia.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>Located a 40 minutes drive from the Streda Belvedare, past the hills of Montalbano's  is Pistoia, a crowded city where modern and ancient buildings flow together, surrounded by ancient Roman walls. In the city center is one of the largest gothic cathedrals in Italy, the Baptisery, the Palace of bishops, and a small square where diverse fruits and vegetables are sold. Every Wednesday and Saturday morning in July and August there is a huge local market with countless unique merchandise,  and during the rest of the year the market is located in the ancient Piazza del Duomo. It is pleasant to linger in Pistoi well into the evening, as this is when the locals emerge, and the city center grows to life with diners enjoying the traditional food and drink. When the weather permits, enjoy lingering at the dinner tables outside as you enjoy your leisurely meal.</p>
			<div class="clear"></div>            

            <h2>The tower of Pisa</h2>
            <img src='images/pisa.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>The tower of Pisa is known throughout the world, and needs no introductions it is simply a sight that you must see during your time in Tuscany and on this world. It is easily reached by train from Empoli, or directly by car by travelling on the Fl Pl Ll highway. </p>
			<div class="clear"></div>   

            <h2>Siena</h2>
            <img src='images/Siena.jpg' width="150" height="150" alt="" class="thingsToDo">
            <p>To reach Siena, it takes 2 hours by car, but only 1 by train from Empoli. You are on vacation: let yourself relax as the train takes you straight to your destination. </p>
			<div class="clear"></div>

            <h2>Other places less than an hour away from the Streda Belvedere </h2>
            <ul>
              <li>
              Certaldo, a picturesque Tuscan town of Roman origin</li>
              <li>San Miniato, which includes the Tower of Frederick, the Diocesan Museum, and many other well preserved medieval attractions</li>
              <li>San Gimignano, the town of towers Lucca, city of 100 churches Slightly more than an hour away is Volterra, the famous Etruscan city</li>
            </ul>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
