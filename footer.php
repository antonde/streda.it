<hr/>
copy; Copyright 2012 Streda Belvedere - Powered by <a href="http://ttsistemi.com">ttsistemi</a>
<div style='float:right;'>
  <a href='index.php'>Home</a> <span style='padding:8px;'>|</span>
  <a href='accomodations.php'>Accommodations</a> <span style='padding:8px;'>|</span>
  <!-- <a>Blog</a> <span style='padding:8px;'>|</span> -->
  <a href='contact.php'>Contact</a>
</div>
<br clear='both'/>
<div style='float:right;font-size:smaller;'>
Streda Belvedere Azienda Agricola<br />
50059 Streda-Vinci (FI) P.Iva 05426360482<br />
Tel+39 0571 729195 Fax +39 0571 568563 E-mail streda@streda.it
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21843021-21']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
