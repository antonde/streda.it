<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Syrah</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Our Wine</a> » Syrah
          </div>        
          <div class='post'>
            <h1>Drufo Syrah di Streda Belvedere</h1>
            <p>
              <h2>Toscana rosso igp(igt)</h2>
            <p>
            <img src='images/wines/syrah.png' style='float:left;'/>
            <b>Charateristics of production area</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Producer</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape Type:</td>
                <td>Syrah</td>
              </tr>
              <tr>
                <td class='tdHeader'>Altitude</td>
                <td>200 metres</td>
              </tr>
              <tr>
                <td class='tdHeader'>Aspect and soil profile</td>
                <td>south ,south- west .Medium textured with calcareous fossil elements</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vineyard density</td>
                <td>4500 vines/hectare</td>
              </tr>
              <tr>
                <td class='tdHeader'>Training method</td>
                <td>Spurred cordon</td>
              </tr>
              <tr>
                <td class='tdHeader'>Average vine age</td>
                <td>15 years</td>
              </tr>
            </table>
            <b>Fermentation and Maturation</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Yield per hectare </td>
                <td>70 quintals of grape</td>
              </tr>
              <tr>
                <td class='tdHeader'>Harvest period</td>
                <td>Mid- September, mechanically harvested</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape processing</td>
                <td>Crusher- destemmer </td>
              </tr>
              <tr>
                <td class='tdHeader'>Temp and length of fermentation</td>
                <td>32°C per 7 days in stainless steel</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maceration on skins</td>
                <td>20 days</td>
              </tr>
             <tr>
                <td class='tdHeader'>Malolactic fermentation</td>
                <td>Yes</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maturation</td>
                <td>12 months in french barriques</td>
              </tr>
              <tr>
                <td class='tdHeader'>Alcohol</td>
                <td>13% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Minimun bottle ageing</td>
                <td>6 months</td>
              </tr>
            </table>
            <b>Tasting notes</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Appearance</td>
                <td>Deep ruby red</td>
              </tr>
              <tr>
                <td class='tdHeader'>Nose</td>
                <td>Floral and fruit aromas well balanced with spice and notes of vanilla</td>
              </tr>
              <tr>
                <td class='tdHeader'>Palate</td>
                <td>Elegant and supple in the mouh, with a rich vein of smooth tannins supported by subtle toasty oak, and the conclusions is very lengthy</td>
              </tr>
              <tr>
                <td class='tdHeader'>Serving suggestions</td>
                <td>Game, roasts, and aged cheeses</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
