<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
<head>
	<title>Streda - Wines</title>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src='js/script.js'></script>
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
	<link href='css/style.css' rel='stylesheet' type='text/css'/>
	<link href='css/apartment.css' rel='stylesheet' type='text/css'/>
	<?php require_once( 'meta.php' ); ?>
</head>

<body>
<div class='bg'></div>
<div class='container'>
	<div class='header'>
		<div class='languageBar'>
			<?php
			include( "language.php" );
			?>
		</div>
		<div class='navigation'>
			<?php
			include( "navbar.php" );
			?>
		</div>
	</div>
	<div class='contentBG'>
		<div class='sidebar' style='left:-30px;top:-20px;'>
			<?php
			include( "sidebarFarm.php" );
			?>
		</div>
		<div class='content'>
			<div class='post'>
				<h1>Streda Wines</h1>

				<p>
					This wine is produced in Streda since the 12th and 13th centuries and during the 15th century, Antonio da Vinci - Leonardo da Vinci’s grandfather - reported to the Register of Land, Property sixteen barrels of wine he had
					made himself on his land along the Streda River and near the nearby Fossato di Vinci.
					Nowadays in Streda the old winemaking traditions are carried on, producing wines which are typical of Tuscany.

				<p>

				<div style='text-align:center;'>
					<img class='nofloat' src='images/wine1.jpg'/>
				</div>

				<h1>Our Red Wines</h1>

				<div style='text-align:center;'>
					<a href='wine_chianti.php'><img class='nofloat' src='images/wine2.png'/></a>
					<a href='wine_sole.php'><img class='nofloat' src='images/wine_sole1.png'/></a>
					<a href='wine_brunello.php'><img class='nofloat' src='images/wine6.png'/></a>
					<a href='wine_casanova.php'><img class='nofloat' src='images/wine4.png'/></a>
					<a href='wine_toiano.php'><img class='nofloat' src='images/wine3.png'/></a>
					<a href='wine_syrah.php'><img class='nofloat' src='images/wine5.png'/></a>
					<br/>
				</div>
				<h1>Other Red and Rosè Wines</h1>
				<div style='text-align:center;'>
					<a href='wine_rosso.php'><img class='nofloat' src='images/wine7.png'/></a>
					<a href='wine_duccio_rose.php'><img class='nofloat' src='images/duccio_rose.png'/></a>

				</div>
				<h1>Our White Wines</h1>

				<div style='text-align:center;'>
					<a href='wine_bianco.php'><img class='nofloat' src='images/wine8.png'/></a>
					<a href='wine_chardonnay.php'><img class='nofloat' src='images/wine9.png'/></a>
				</div>

			</div>
		</div>
		<br clear='both'/>
	</div>
	<div class='footer'>
		<?php
		include( "footer.php" );
		?>
	</div>
</div>
</body>
</html>
