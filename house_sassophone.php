<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Sassophone</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/sassophone/image1.jpg',
          'images/sassophone/image2.jpg',
          'images/sassophone/image3.jpg',
          'images/sassophone/image4.jpg',
          'images/sassophone/image5.jpg',
          'images/sassophone/image6.jpg',
          'images/sassophone/image7.jpg',
          'images/sassophone/image8.jpg',
          'images/sassophone/image9.jpg',
          'images/sassophone/image10.jpg',
        ]);
      });
      imgDir = "sassophone";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <?php
            include("sidebarHousing.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php'>Accomodations</a> » Sassofono
          </div>
          <h1>Sassofono</h1>
        <div class='imagePreview'>
          <img id='newImage' src='images/sassophone/image1.jpg' class='previewLoader'/>
          <img id='oldImage' src='images/sassophone/image1.jpg'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/sassophone/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/sassophone/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/sassophone/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/sassophone/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/sassophone/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/sassophone/image6.png' onclick='changePreview(6)' /></li>
                <li><img src='images/sassophone/image7.png' onclick='changePreview(7)' /></li>
                <li><img src='images/sassophone/image8.png' onclick='changePreview(8)' /></li>
                <li><img src='images/sassophone/image9.png' onclick='changePreview(9)' /></li>
                <li><img src='images/sassophone/image10.png' onclick='changePreview(10)' /></li>
                <li><img src='images/sassophone/image11.png' onclick='changePreview(11)' /></li>
                <li><img src='images/sassophone/image12.png' onclick='changePreview(12)' /></li>
                <li><img src='images/sassophone/image13.png' onclick='changePreview(13)' /></li>
                <li><img src='images/sassophone/image14.png' onclick='changePreview(14)' /></li>
                <li><img src='images/sassophone/image15.png' onclick='changePreview(15)' /></li>
              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>Unit Details:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Type of accomodations: </td>
                <td>2-rooms apartment, 44m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of beds :</td>
                <td>4 (2+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bedrooms :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bathrooms :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Floor / terrace :</td>
                <td>Ground floor; private patio (30 mq) with table, chairs and deckchair.   </td>
              </tr>
              <tr>
                <td class='tdHeader'>Location :</td>
                <td>Countryside with a shared garden of 5000 m² in a property of 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composed of :</td>
                <td>Living room – dining room with a double sofa bed and cooking area, 1 double bedroom (queen size bed with MEMOREX foam), 1 bathroom with shower,</td>
              </tr>
              <tr>
                <td class='tdHeader'>Kitchen corner equipped with :</td>
                <td>Dishwasher, refrigerator, freezer, 4 kitchen stoves (gas) and a toaster.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Apartment equipped with :</td>
                <td>Air conditioning, Heating, LCD digital TV, and hairdryer./td>
              </tr>
              <tr>
                <td class='tdHeader'>View :</td>
                <td>Countryside</td>
              </tr>
              <tr>
                <td class='tdHeader'>Access of disabled / bathroom for disabled :</td>
                <td>Yes</td>
              </tr>
            </table>
            
            
			<h2>Sassofono Plus - This room can be combined with <a href="house_camera2.php" target="_blank">Orange Room</a>:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Type of accomodations: </td>
                <td>3-rooms apartment, 68m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of beds :</td>
                <td>6 (4+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bedrooms :</td>
                <td>2 (1 is on the first floor with a separate entrance and without connecting door).</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bathrooms :</td>
                <td>2</td>
              </tr>
              <tr>
                <td class='tdHeader'>Floor / terrace :</td>
                <td>ground floor and the 1st floor; private patio (40 mq) with table, chairs and deckchair.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Location :</td>
                <td>Countryside with a shared garden of 5000 m² in a property of 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composed of :</td>
                <td>On the ground floor :living/dining room with a double sofa bed and kitchenette, 1 double bedroom (queen size bed with MEMOREX foam), 1 bathroom with shower; On the first floor (with a separate entrance and without connecting door):1 double bedroom (king size bed with MEMOREX foam), 1 bathroom with shower.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Kitchen corner equipped with :</td>
                <td>Dishwasher, refrigerator, freezer, 4 kitchen stoves (gas) and a toaster.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Apartment equipped with :</td>
                <td>Air conditioning, Heating, LCD digital TV, and hairdryer.</td>
              </tr>
              <tr>
                <td class='tdHeader'>View :</td>
                <td>Countryside</td>
              </tr>
              <tr>
                <td class='tdHeader'>Access of disabled / bathroom for disabled :</td>
                <td>No</td>
              </tr>
            </table>            
            <p>* Please note that the apartment Saxophone is on the ground floor while the room Orange is on the first floor and has another entrance. There is no connection door. For this reason the apartment is only appropriate for adults or families with independent children.</p>

            <h2>Swimming Pool:</h2>
            <img src='images/pool1.jpg'/>
            <table>
              <tr>
                <td class='tdHeader'>Dimensions :</td>
                <td>14m x 7m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Depth :</td>
                <td>Minimum 1m; maximum 1.40m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Opening period :</td>
                <td>From March 31 till October 13</td>
              </tr>
              <tr>
                <td class='tdHeader'>Opening hours :</td>
                <td>From 8.30h until 18.30h</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pool cleaning :</td>
                <td>Daily</td>
              </tr>
              <tr>
                <td class='tdHeader'>Whirlpool :</td>
                <td>Shared, dimensions 2,20m x 2,20m, deep 0,50 m</td>
              </tr>
            </table>

            <h2>Services:</h2>
            A laundry room with washing machine, dryer and iron, free wi-fi, parking place, a children’s playing area (in construction, ready for the season 2012), 2 shared barbecues and an outside shower in the swimming pool.
            <br/><br/>
            <a href='reservation.php?unit=Sassophone'><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
