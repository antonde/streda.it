<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Contact</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
     <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
     <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    
	<?php require_once('meta.php'); ?>
	<script>
	    $(function() {
		    $( "#arriving" ).datepicker();
		    $( "#departing" ).datepicker();
	    });
	</script>
	<script type="text/javascript">
    $(document).ready(function() {
      $("#contact_form").validate({
        rules: {
          Name: "required",//name field validate
          Email: {// compound rule
	          required: true,
	          email: true
	      },
	      PhoneNumber: {
		      required: true,
		  },
		  Arriving: {
			  required: true
		  },
		  Departing: {
			  required: true
		  }
        },
        messages: {
            message: "Please enter a comment.",
  		    name:"Please enter your name."
  		}
      });
    });

  </script>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebar.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
          	<h2>General Inquires</h2><br>

          	
            <h1>Request Availability</h1>
                        
            <form name='form' id="contact_form" method="post" action="verify.php">
            	<table class='form'>
                <tr>
                  <td>Email Address *</td>
                  <td><input type='text' name='Email'/></td>
                </tr>
                <tr>
                  <td>Name *</td>
                  <td><input type='text' name='Name'/></td>
                </tr>
                <tr>
                  <td>Surname</td>
                  <td><input type='text' name='Surname'/></td>
                </tr>
                <tr>
                  <td>City</td>
                  <td><input type='text' name='City'/> CAP: <input type='text' name='CAP'/></td>
                </tr>
                <tr>
                  <td>Country</td>
                  <td><input type='text' name='Country'/></td>
                </tr>
                <tr>
                  <td>Phone Number *</td>
                  <td><input type='text' name='PhoneNumber'/></td>
                </tr>
                <tr>
                  <td>Mobile/Cell Number</td>
                  <td><input type='text' name='FAXNumber'/></td>
                </tr>
                <tr>
                  <td>Period *</td>
                  <td>
                    from <input type="text" name="Arriving" id='arriving'/>
                    to <input type='text' name='Departing' id='departing'/>
                  </td>
                </tr>
                <tr>
                  <td>Number of Adults</td>
                  <td><input type='text' name='NumberofAdults'/></td>
                </tr>
                <tr>
                  <td>Number of kids under 13</td>
                  <td><input type='text' name='Numberofkidsunder13'/></td>
                </tr>
                <tr>
                  <td>Unit Requested</td>
                  <td><input type='text' name='Unit' value='<?=$_GET["unit"];?>'/></td>
                </tr>
                <tr>
                  <td>Special Requests</td>
                  <td><textarea name='SpecialRequests' style='width:90%;min-height:120px;'></textarea></td>
                </tr>
                <tr>
                  <td style='text-align:center;'><input type='checkbox' onclick='document.getElementById("submit").disabled=false'/> * </td>
                  <td>Consent to submit personal information</td>
                </tr>
              </table>
              <p>
                N.B. Consent is required to access this website. By submitting this form interested parties acknowledge to be aware of information and rights as provided for by Italian law.
              <p>
              <div style='text-align:center;'>
                <input type='submit' value='Submit' id='submit' disabled='disabled'/>
              </div>
			 <div style="padding: 10px; display: inline !important;"> 
			 <?php
				require_once('recaptchalib.php');
				$publickey = "6LcIC_ASAAAAABmu1jvxQPSjOhXmI6AuZSdvpZlJ";
				echo recaptcha_get_html($publickey);
		    ?>
			</div>
          	
            </form>
            *REQUIRED FIELDS <br>
            <p>
            Streda Belvedere Azienda Agricola</br>
			50059 Streda-Vinci (FI)</br>
			Tel+39 0571 729195 Fax +39 0571 568563</br> 
			E-mail streda@streda.it</br>
            </p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
