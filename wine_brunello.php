<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Brunello di Montalcino</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='wine.php'>Our Wine</a> » Brunello di Montalcino
          </div>
          <div class='post'>
            <h1>Brunello di Montalcino</h1>
            <p>
              <h2>Streda Belvedere dop(docg)</h2>
            <p>
            <img src='images/wines/brunello.png' style='float:left;'/>
            <b>Charateristics of production area</b>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Producer</td>
                <td>Streda Belvedere</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape Type:</td>
                <td>sangiovese grosso</td>
              </tr>
              <tr>
                <td class='tdHeader'>Altitude</td>
                <td>350 metres</td>
              </tr>
              <tr>
                <td class='tdHeader'>Aspect and soil profile</td>
                <td>Sout-east. Medium textured gravel-pebbles</td>
              </tr>
              <tr>
                <td class='tdHeader'>Vineyard density</td>
                <td>4500 vines/hectare</td>
              </tr>
              <tr>
                <td class='tdHeader'>Training method</td>
                <td>Spurred cordon</td>
              </tr>
              <tr>
                <td class='tdHeader'>Average vine age</td>
                <td>20 years</td>
              </tr>
            </table>
            <b>Fermentation and Maturation</b><br/>
            <table style='width:65%;'>
              <tr>
                <td class='tdHeader'>Yield per hectare </td>
                <td>80 quintals of grape</td>
              </tr>
              <tr>
                <td class='tdHeader'>Harvest period</td>
                <td>septmber, october Harvested manually</td>
              </tr>
              <tr>
                <td class='tdHeader'>Grape processing</td>
                <td>Crusher, de-stemmer </td>
              </tr>
              <tr>
                <td class='tdHeader'>Temp and length of fermentation</td>
                <td>32°C per 7 days in stainless steel</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maceration on skins</td>
                <td>7 days</td>
              </tr>
             <tr>
                <td class='tdHeader'>Malolactic fermentation</td>
                <td>Yes</td>
              </tr>
              <tr>
                <td class='tdHeader'>Maturation</td>
                <td>Must undergo a refining period of at least four months in bottles; the release for purchase can only be made five years after the vintage year</td>
              </tr>
              <tr>
                <td class='tdHeader'>Alcohol</td>
                <td>12.5% vol</td>
              </tr>
              <tr>
                <td class='tdHeader'>Minimun bottle ageing</td>
                <td>4 months</td>
              </tr>
            </table>
            <b>Tasting notes</b><br/>
            <table class='descriptive'>
              <tr>
                <td class='tdHeader'>Appearance</td>
                <td>Garnet-edged red</td>
              </tr>
              <tr>
                <td class='tdHeader'>Nose</td>
                <td>Floral and fruit, well balances</td>
              </tr>
              <tr>
                <td class='tdHeader'>Palate</td>
                <td>Smooth and rounded on the palate, with soft tannins and an excellent finish. Overall a very harmonious wine</td>
              </tr>
              <tr>
                <td class='tdHeader'>Serving suggestions</td>
                <td>First courses, and every kind of salad</td>
              </tr>
            </table>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
