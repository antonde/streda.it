<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Grappa</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarFarm.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>Grappa del Chianti</h1>
            <h1> GOCCE DI LUNA</h1>
            <img src='images/grappa1.png'/>
            <p>
              Our grappa is made from the aromatic skins, stalks and pips from DOCG Chianti grapes from the Streda Estates. Its fermentation keeps the fragrance of the original grape blend intact, enhanced by distillation in underground wine cellars.
              <br/><br/>
              <b>Organoleptic characteristics:</b><br/>
              Flavors ranging from floral to fruity - slightly aromatic. 
              <br/><br/>
              <b>Taste:</b><br/>
              Light and smooth
              <br/><br/>
              <b>Alcohol</b><br/>
              40°
              <br/><br/>
              <b>Bottles</b><br/>
              Size/ml 700
            <p>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
