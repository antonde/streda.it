<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Chardonnay</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
    <script>
      $(document).ready(function() {
        preload([
          'images/chardonnay/image1.jpg',
          'images/chardonnay/image2.jpg',
          'images/chardonnay/image3.jpg',
          'images/chardonnay/image4.jpg',
          'images/chardonnay/image5.jpg',
          'images/chardonnay/image6.jpg',
          'images/chardonnay/image7.jpg',
          'images/chardonnay/image8.jpg',
          'images/chardonnay/image9.jpg',
          'images/chardonnay/image10.jpg',
        ]);
      });
      imgDir = "chardonnay";
    </script>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
           include("navbar.php");
          ?>
        </div>
      </div>
      <div class='contentBG'>
        <div class='sidebar'>
          <?php
            include("sidebarHousing.php");
          ?>
        </div>
        <div class='content'>
          <div class='breadcrumb'>
            <a href='accomodations.php'>Accomodations</a> » Chardonnay
          </div>
          <h1>Chardonnay</h1>
        <div class='imagePreview'>
          <img id='newImage' src='images/chardonnay/image1.jpg' class='previewLoader'/>
          <img id='oldImage' src='images/chardonnay/image1.jpg'/>
        </div>
        <div style='position:relative;'>
          <img onclick='scrollToTheLeft()' src='images/leftArrow.png' class='scroller'/>
          <img onclick='scrollRight()' src='images/rightArrow.png' class='scroller right'/>
          <div class='thumbnailSlider'>
            <div id='thumbContainer' class='thumbnailContainer'>
              <ul id='thumbnails' class='thumbnails'>
                <li><img src='images/chardonnay/image1.png' onclick='changePreview(1)' /></li>
                <li><img src='images/chardonnay/image2.png' onclick='changePreview(2)' /></li>
                <li><img src='images/chardonnay/image3.png' onclick='changePreview(3)' /></li>
                <li><img src='images/chardonnay/image4.png' onclick='changePreview(4)' /></li>
                <li><img src='images/chardonnay/image5.png' onclick='changePreview(5)' /></li>
                <li><img src='images/chardonnay/image6.png' onclick='changePreview(6)' /></li>
                <li><img src='images/chardonnay/image7.png' onclick='changePreview(7)' /></li>
                <li><img src='images/chardonnay/image8.png' onclick='changePreview(8)' /></li>
                <li><img src='images/chardonnay/image9.png' onclick='changePreview(9)' /></li>
                <li><img src='images/chardonnay/image10.png' onclick='changePreview(10)' /></li>
                <li><img src='images/chardonnay/image11.png' onclick='changePreview(11)' /></li>

              </ul>
            </div>
          </div>
        </div>
          <div class='post'>
            <h2>Unit Details:</h2>
            <table>
              <tr>
                <td class='tdHeader'>Type of accomodations: </td>
                <td>2-rooms apartment, 58m²</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of beds :</td>
                <td>4 (2+2)</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bedrooms :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Number of bathrooms :</td>
                <td>1</td>
              </tr>
              <tr>
                <td class='tdHeader'>Floor / terrace :</td>
                <td>Ground floor, private patio and garden (about 30 sqmt) with table, chairs and deckchair.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Location :</td>
                <td>Countryside with a shared garden of 5000 m² in a property of 80ha.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Composed of :</td>
                <td>Living room – dining room with a double sofa bed,  kitchen, 1 double bedroom (king size bed with MEMOREX foam), 1 bathroom with shower.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Kitchen corner equipped with :</td>
                <td>Dishwasher, refrigerator, freezer, 4 kitchen stoves (gas) and a toaster.</td>
              </tr>
              <tr>
                <td class='tdHeader'>Apartment equipped with :</td>
                <td>Air conditioning, heating, LCD digital TV, hairdryer, free Wi-Fi and cotton linens.</td>
              </tr>
              <tr>
                <td class='tdHeader'>View :</td>
                <td>Countryside</td>
              </tr>
              <tr>
                <td class='tdHeader'>Access of disabled / bathroom for disabled :</td>
                <td>No</td>
              </tr>
            </table>

            <h2>Swimming Pool:</h2>
            <img src='images/pool1.jpg'/>
            <table>
              <tr>
                <td class='tdHeader'>Dimensions :</td>
                <td>14m x 7m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Depth :</td>
                <td>Minimum 1m; maximum 1.40m</td>
              </tr>
              <tr>
                <td class='tdHeader'>Opening period :</td>
                <td>From March 31 till October 13</td>
              </tr>
              <tr>
                <td class='tdHeader'>Opening hours :</td>
                <td>From 8.30h until 18.30h</td>
              </tr>
              <tr>
                <td class='tdHeader'>Pool cleaning :</td>
                <td>Daily</td>
              </tr>
              <tr>
                <td class='tdHeader'>Whirlpool :</td>
                <td>Shared, dimensions 2,20m x 2,20m, deep 0,50 m</td>
              </tr>
            </table>

            <h2>Services:</h2>
            A laundry room with washing machine, dryer and iron, free wi-fi, parking place, a children’s playing area (in construction, ready for the season 2012), 2 shared barbecues and an outside shower in the swimming pool.
            <br/><br/>
            <a href='reservation.php?unit=Chardonnay'><img src='images/requestUnit.png'></a>
          </div>
        </div>
        <br clear='all'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
