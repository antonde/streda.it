function showMenu(){
  $("#farmMenu").stop();
  $("#farmMenu").animate( {height:220}, 500);
}

function hideMenu(){
  $("#farmMenu").stop();
  $("#farmMenu").animate( {height:0}, 500);
}

position = 0;

function scrollToTheLeft(){
  position += 85;
  scroll();
}
function scrollRight(){
  position -= 85;
  scroll();
}

function scroll(){
  maxWidth = Math.ceil($("#thumbnails").width() / 85) * 85;

  if(position>0){
    position=0;
  }
  if(maxWidth + position < 516){
    position = - (maxWidth - 516) - 6;
  }
  $("#thumbContainer").animate( {left:position}, 400, "linear");        
}

function changePreview(image){
  $("#oldImage").css("opacity", 1);
  $("#newImage").attr("src", "images/"+imgDir+"/image"+image+".jpg");
  $("#oldImage").animate( {opacity:0}, 800 , "linear", doneFading);
}
function doneFading(){
  $("#oldImage").attr("src", $("#newImage").attr("src"));
  $("#oldImage").css("opacity", 1);
}
function preload(arrayOfImages) {
  $(arrayOfImages).each(function(){
    $("<img/>")[0].src = this;
  });
}
