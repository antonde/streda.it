<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
  <head>
    <title>Streda - Products</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src='js/script.js'></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <link href='css/apartment.css' rel='stylesheet' type='text/css'/>
<?php require_once('meta.php'); ?>
  </head>

  <body>
    <div class='bg'></div>
    <div class='container'>
      <div class='header'>
        <div class='languageBar'>
          <?php
            include("language.php");
          ?>
        </div>
        <div class='navigation'>
          <?php
            include("navbar.php");
          ?>
        </div>
      </div>

      <div class='contentBG'>
        <div class='sidebar' style='left:-30px;top:-20px;'>
          <?php
            include("sidebarProd.php");
          ?>
        </div>
        <div class='content'>
          <div class='post'>
            <h1>Our Products</h1>
            <p>
              Provide olive oil and home wine and suddenly the people of Tuscany have a feast at their table.<br/>
              This is customary for every Mediterranean civilization, but in Tuscany - land of the Etruscans and of the creators of the Renaissance - something has tied together wines, olive trees and vines to the art of fine cooking.<br/>
              An exchange and symbiosis occurs between these two instruments of happiness at the table.<br/>
              By now in Tuscany, wine and olive oil have become an inseparable part of our history and culture.<br/>
            <p>
            <div style='text-align:center;'>
              <a href='wine.php'><img class='nofloat' src='images/product1.jpg'/></a>
              <br/><br/>
              <a href='grappa.php'><img class='nofloat' src='images/product2.jpg'/></a>
              <br/><br/>
              <a href='oil.php'><img class='nofloat' src='images/product3.jpg'/></a>
              <br/><br/>
              <a href='http://blog.streda.it/category/award-review/'><img class='nofloat' src='images/product4.jpg'/></a>
            </div>
          </div>
        </div>
        <br clear='both'/>
      </div>
      <div class='footer'>
        <?php
          include("footer.php");
        ?>
      </div>
    </div>
  </body>
</html>
